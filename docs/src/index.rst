============
Introduction
============

The Sensitivity Calculator provides functionality for SKA scientists and engineers to
estimate sensitivities and integration times for observations on both SKA LOW and MID.

This document describes the theoretical background, the current Calculator
implementation, explaining the decisions behind its design, and links to a
space where future work can be planned.

For more details on developing and deploying the code, see the `README <https://gitlab.com/ska-telescope/ost/ska-ost-senscalc/-/blob/master/README.md>`_


.. Hidden toctree to manage the sidebar navigation.

.. Overview section
.. toctree::
   :maxdepth: 1
   :caption: OVERVIEW
   :hidden:

   motivation

.. MID section
.. toctree::
   :maxdepth: 1
   :caption: MID
   :hidden:

   mid_background
   mid_implementation
   mid_rest_api
   future_work

.. LOW section
.. toctree::
   :maxdepth: 1
   :caption: LOW
   :hidden:

   low_background
   low_implementation
   low_rest_api

.. API section
.. toctree::
   :maxdepth: 1
   :caption: Public API Documentation
   :hidden:

   package/mid
   package/subarray
   package/utilities
   package/mid_utilities
   package/low_api
   package/low_validation
   package/low_model
   package/low_service
   package/low_calculator
   package/low_lookup
