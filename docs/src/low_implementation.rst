.. _low_implementation:

===================
LOW Implementation
===================

The LOW Sensitivity Calculator follows the basic structure of an API layer (see LOW API docs page), validation,
then calculation, using a look up table. The code is functional in nature, relying on pure functions which do not mutate state.
The diagram below aims to give a view of this functional structure.

.. figure:: diagrams/export/function_view.svg
   :align: center


The Python modules can also been seen below. Mostly the implementation uses module level pure functions, and makes use of
classes only for data objects and to encapsulate the look up tables.


.. figure:: diagrams/export/module_diagram.svg
   :align: center
