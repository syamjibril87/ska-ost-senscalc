.. _mid_implementation:

===================
MID Implementation
===================


.. image:: _static/img/backend_uml.png

**Figure 4** . Class diagram of the Sensitivity Calculator back-end.

The back-end is written in Python 3.7 and the class diagram is shown in 
Fig.3. The class *MidCalculator* has 2 public methods:  
*calculate_sensitivity* to get the array sensitivity in 
Jy for the given integration time, and *calculate_integration_time*
to get the integration time required for the given sensitivity. 

The *MidCalculator* constructor has a number of required parameters
that define the observing configuration, target and weather. The
rest default to None, in which case their 
values will be calculated automatically. The automatic values can
be overriden by setting them here. 
 
All parameters, internal variables and results that describe 'physical' 
measures are implemented as astropy Quantities to prevent mixups over
units.

The calculator is modular in design. 
There are separate functions for deriving each element of the 
calculation, which can be easily modified as the 
sensitivity model is updated.
