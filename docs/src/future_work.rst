.. _future_work:

============
Further Work
============

The calculator, in its current prototype state, is the product of 6 months 
of work by one developer. It is intended to be a sufficient platform to 
allow the development of a more sophisticated tool. 
There are already several suggestions for work that needs to be done in
the future. These are grouped below according to their source.

SKA Engineering
'''''''''''''''
It has been pointed out that SKA Engineers and Astronomers define 
'sensitivity' differently, and there is some question whether
the Sensitivity Model used by the Calculator matches that already
in use by the Engineers. We need to ensure that we define and use
our terms carefully and that the sensitivity models are reconciled.  

Ideas from the Prototype
''''''''''''''''''''''''
These ideas cropped up during development of the prototype, but were
considered too time-consuming or too far off in the future to be added 
at that stage:

- **Shadowing.** 
  In reality, depending on the pointing direction of the dishes in the array, 
  the dishes are likely to obscure one another, resulting in a loss of 
  effective area. This can be accounted for by determining a shadowing 
  fraction (i.e. what proportion of the total area is shadowed) and 
  reducing the effective area by a proportional amount.

- **Image weighting.**
  The type of image weighting used will affect the sensitivity one can 
  achieve with the observation. Incorporating the Briggs robust weighting 
  parameter into the calculation will help reflect this.

- **Beam synthesis.**
  Running some simulations to synthesise beams would be incredibly useful 
  and open up a lot of other options for functionality for the calculator.

- **Weather.**
  Currently the user is given the options of "Good", "Average" and "Bad" 
  weather, corresponding to pwv values of 5.8mm, 10.7mm and 19.2mm 
  respectively. While this is important for the calculation of the 
  atmospheric temperature, :math:`T_{atm}`, it is impossible for the user to predict 
  what the weather is going to be like when their observation gets scheduled. 
  Instead, it may make more sense to give options for different months/seasons, 
  since then the user would at least get an idea what the weather conditions 
  will likely be over the time their observations could be scheduled.

- **Optional smoothing for zooms.**
  Down the line it is probably a good idea to add a optional line smoothing 
  option for zooms.

- **More observing modes.**
  The calculator currently sports two observing modes - continuum and line 
  observations. As it is developed, it would be good to have more observing 
  modes added. The prototype has a tab for pulsar observations (and some 
  comments throughout the code), but there is nothing yet implemented for 
  this mode - it is just a placeholder/suggestion.

- **Report resources.**
  Adding some report of the resources that will be used for the observation 
  (e.g. compute time) would be a valuable addition to the calculator output.

- **Populate inputs from URL.**
  A handy feature would be if the calculator would parse the query string 
  from the URL and preload the calculator inputs with those values. When 
  combined with a 'link generator' feature which would be fairly 
  straightforward to add, this would allow users to generate links to the 
  calculations they have performed and share them with colleagues. When the 
  colleague clicked the link/pasted it into their address bar, they would be 
  taken to the page and the inputs would be loaded with the same values the 
  first user had used.

- **Other Calculators.**
  In developing this calculator, it was useful to regularly look at other, 
  similar calculators/tools which exist. These other tools helped inform 
  design and inspire new feature ideas. A list of such calculators follows 
  here, which will hopefully be of use as the calculator is further developed.

  - `ALMA Sensitivity Calculator <https://almascience.eso.org/proposing/sensitivity-calculator>`_
  - `ATCA Sensitivity Calculator <https://www.narrabri.atnf.csiro.au/myatca/interactive_senscalc.html>`_
  - `e-MERLIN Sensitivity Calculator <http://www.e-merlin.ac.uk/calc.html>`_
  - `VLA Exposure Calculator <https://obs.vla.nrao.edu/ect/>`_


The Vision Thing
''''''''''''''''
Would it be worth asking some people to write a (very) short story
describing how they imagine they would use the SKA 'in the ideal world', 
especially
with reference to the Sensitivity Calculator?
Consider different scenarios
e.g. standard observing, response to transient triggers, survey planning,
whatever you can think of.

