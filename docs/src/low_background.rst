.. _low_background:

===========================
LOW Theoretical Background
===========================

TODO - a background on the calculation and the science behind it.
See the MID Theoretical Background for inspiration.

Dynamic range warning
=====================

In addition to calculating the theoretical sensitivity,
the LOW sensitivity calculator backend warns if it detects
a bright (>10 Jy) off-axis source within the full width at
half maximum (FWHM) of the LOW station beam calculated at
the centre of the specified band. The backend uses a bright
source catalogue derived from the GaLactic and Extragalactic
All-sky Murchison Widefield Array (GLEAM) survey
(`Hurley-Walker et al. (2017) <https://ui.adsabs.harvard.edu/link_gateway/2017MNRAS.464.1146H/doi:10.1093/mnras/stw2337>`_).

