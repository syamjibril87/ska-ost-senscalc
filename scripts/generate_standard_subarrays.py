"""Generate files that describe standard subarrays when associated with the given configuration

Run as e.g.

    python generate_standard_subarrays.py --configuration_path some/path/to/ska1mid.cfg
"""

#!/usr/bin/python

import argparse
import os
import hashlib
import json
import numpy as np
from astropy.io import ascii


def cli_parser():
    """Get a command line parser and populate it with arguments

    First a CLI argument parser is created. Each function call adds more arguments to the parser.

    :return: CLI parser argparse
    """

    parser = argparse.ArgumentParser(
        description="Generate standard subarrays from configuration file",
        fromfile_prefix_chars="@",
    )

    parser.add_argument(
        "--configuration_path",
        type=str,
        default="ska1mid.cfg",
        help="Path to configuration file",
    )

    return parser


def generate_subarrays(configuration_path="ska1mid.cfg"):
    """Utility method to generate index files that will specify standard 
    subarray types when associated with the given configuration file.
    The three files produced are: full.json with all antennas,
    SKA1 (133 x 15m).json with all SKA1 antennas, and meerKAT.json with all Meerkat
    antennas.

    :param configuration: the name of the configuration file to be used.
    :type configuration: str
    """

    # calculate the checksum of the configuration file

    f = open(configuration_path, "rb")
    content = f.read()
    hashlib.md5().update(content)
    digest = hashlib.md5().hexdigest()
    f.close()

    # read the configuration file as an astropy table.

    cols = ["X", "Y", "Z", "Diam", "Station"]

    df = ascii.read(
        configuration_path,
        delimiter=" ",
        comment="#",
        names=cols,
    )

    # adding the ids column

    df.add_column(np.arange(len(df)), name='ID', index=0)
    cols.insert(0, "ID")

    # adding in indexing as this does not work by default with astropy
    df.add_index(cols)

    # find the ids for standard subarrays and save them to file

    configuration = os.path.basename(configuration_path)
    ska_ids = [i for i, idx in enumerate(df["Station"].tolist()) if idx[0] == "S"]

    f = open("SKA1 (133 x 15m).json", "w")
    json.dump(
        {
            "ids": ska_ids,
            "name": "SKA1 (133 x 15m)",
            "configuration": configuration,
            "md5_checksum": digest,
        },
        f,
    )
    f.close()

    meerkat_ids = [i for i, idx in enumerate(df["Station"].tolist()) if idx[0] == "M"]
    f = open("meerKAT.json", "w")
    json.dump(
        {
            "ids": meerkat_ids,
            "name": "meerKAT",
            "configuration": os.path.basename(configuration),
            "md5_checksum": digest,
        },
        f,
    )
    f.close()

    full_ids = [i for i, idx in enumerate(df["Station"].tolist()) if idx[0] in ["M", "S"]]
    f = open("full.json", "w")
    json.dump(
        {
            "ids": full_ids,
            "name": "full",
            "configuration": configuration,
            "md5_checksum": digest,
        },
        f,
    )
    f.close()


if __name__ == "__main__":
    parser = cli_parser()
    args = parser.parse_args()
    generate_subarrays(args.configuration_path)

