import os
import subprocess

import numpy as np
from astropy.io import ascii
from astropy.table import Table, vstack


def calculate(table_name="beam_weighting_sensitivity", debug_table=False):
    """This script uses the rascil_sensitivity app to generate a table
    of point source sensitivities for a range of beam-weighting methods,
    target declinations, elevations, and integration times (track lengths)

    :param table_name: the root name of the table to be constructed.
    :type table_name: str
    :param debug_table: True for a table over a restricted range of parameters.
    :type debug_table: bool

    Before running, install rascil and define RASCIL and RASCIL_DATA
    environment variables.
    """

    # the rascil_sensitivity app

    # usage: rascil_sensitivity.py [-h] [--use_dask USE_DASK]
    #                         [--configuration CONFIGURATION]
    #                         [--subarray SUBARRAY]
    #                         [--imaging_npixel IMAGING_NPIXEL]
    #                         [--imaging_cellsize IMAGING_CELLSIZE]
    #                         [--imaging_weighting IMAGING_WEIGHTING]
    #                         [--imaging_robustness [IMAGING_ROBUSTNESS ...]]
    #                         [--imaging_taper [IMAGING_TAPER ...]] [--ra RA]
    #                         [--tsys TSYS] [--efficiency EFFICIENCY]
    #                         [--diameter DIAMETER] [--declination DECLINATION]
    #                         [--rmax RMAX] [--frequency FREQUENCY]
    #                         [--integration_time INTEGRATION_TIME]
    #                         [--time_range TIME_RANGE TIME_RANGE]
    #                         [--nchan NCHAN] [--channel_width CHANNEL_WIDTH]
    #                         [--verbose VERBOSE] [--results RESULTS]

    # delete final destination for look-up table if it exists
    outfile = "{table_name}.csv".format(table_name=table_name)
    if os.path.exists(outfile):
        os.system("rm %s" % outfile)

    # parameters
    frequency = 1.4e9  # every radio astronomer's favourite frequency, in Hz
    obsLength = 1.0  # total length of observation, in hours
    avgTime = 600.0  # correlator averaging time, in seconds
    nChans = [1, 30]  # number of channels used in spectral-line and continuum
    chanWidth = frequency * 0.01  # channel separation in continuum mode
    robusts = [-2, -1, 0, 1, 2]

    # use small parameter space for debugging
    # tapering values are in arcsec
    if debug_table:
        subarrays = ["SKA1 (133 x 15m).json", "meerKAT.json"]
        declinations = [10, -70]
        tapers = np.array([0.25, 32.0])
        weighting = ['robust']
    else:
        subarrays = ["full.json", "SKA1 (133 x 15m).json", "meerKAT.json"]
        declinations = [44, 30, 10, -10, -30, -50, -70, -90]
        tapers = np.array([0.0, 0.25, 0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0,
                           64.0, 128.0, 256.0, 512.0, 1024.0, 1280.0])
        weighting = ['uniform', 'robust', 'natural']

    # convert tapering values to radians
    tapersR = np.deg2rad(tapers / 3600.)

    # delete pre-existing temporary results if they're there for some reason
    if os.path.exists("temporary_sensitivity.csv"):
        os.remove("temporary_sensitivity.csv")

    # set merged_table to None so that we know we're on the first loop
    merged_table = None

    # loop over grid, run the app for each point, merge the results
    for subarray in subarrays:
        for declination in declinations:
            for nChan in nChans:
                for weight in weighting:

                    print(
                        "subarray={subarray} "
                        "weighting={weight} "
                        "nchans={nChan} "
                        "dec={declination}".format(
                            subarray=subarray,
                            weight=weight,
                            nChan=nChan,
                            declination=declination,
                        )
                    )

                    # will assume that all simulations are done at transit
                    ha = 0.0
                    time_range = [
                        ha - obsLength / 2.,
                        ha + obsLength / 2.,
                    ]

                    # set up arguments
                    command = [
                        "python3 $RASCIL/rascil/apps/rascil_sensitivity.py "
                        "--configuration MID "
                        "--subarray '{subarray}' "
                        "--frequency {frequency} "
                        "--declination {declination} "
                        "--integration_time {avgTime} "
                        "--time_range {time_range[0]} {time_range[1]} "
                        "--imaging_npixel 1024 "
                        "--imaging_weighting {weight} "
                        "--nchan {nChan} "
                        "--channel_width {chanWidth} "
                        "--results temporary".format(
                            subarray=subarray,
                            frequency=frequency,
                            declination=declination,
                            avgTime=avgTime,
                            time_range=time_range,
                            weight=weight,
                            nChan=nChan,
                            chanWidth=chanWidth,
                        )
                    ]

                    # Insert robust values into arguments
                    robustString = " --imaging_robustness " \
                        + " ".join([str(x) for x in robusts])
                    command[0] = command[0] + robustString

                    # Insert tapering values into arguments
                    taperString = " --imaging_taper " \
                        + " ".join([str(x) for x in tapersR])
                    command[0] = command[0] + taperString

                    # Uncomment this to see the actual command that will be run
                    # print(command)

                    # run rascil_sensitivity - using Popen as it
                    # redirects output and CTRL-Y works to abort
                    # the called script and this method - on a Mac
                    # anyway
                    pipe = subprocess.Popen(
                        command,
                        shell=True,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE,
                    )
                    res = pipe.communicate()

                    # read results from this run of the sensitivity app
                    table = ascii.read("temporary_sensitivity.csv")

                    # add important parameters to table
                    table["subarray"] = subarray
                    table["declination"] = declination
                    if nChan == 1:
                        table["spec_mode"] = 'line'
                    else:
                        table["spec_mode"] = 'continuum'

                    # need to add a column for the tapering in arcsec
                    # this makes it easier to extract the right rows
                    # needs to be done for each value of robust
                    if weight == 'robust':
                        table["taper_arcsec"] = np.repeat(tapers,
                                                          len(robusts))
                    else:
                        table["taper_arcsec"] = tapers

                    # table["time_start"] = time_range[0]
                    # table["time_end"] = time_range[1]

                    # add this loop's table to the master table if it exists
                    # otherwise, create it
                    if merged_table is not None:
                        merged_table = vstack([merged_table, table])
                    else:
                        merged_table = Table(table, copy=False)

                    # clean up
                    os.system("rm temporary_sensitivity.csv")

    # write master table to disk
    ascii.write(merged_table, outfile, delimiter=',', overwrite=True)

    # list columns of interest
    print(merged_table.columns)
    print(merged_table)
    print(
        merged_table[
            [
                "subarray",
                "declination",
                "weighting",
                "robustness",
                "spec_mode",
                "taper_arcsec",
                "cleanbeam_bmaj",
                "cleanbeam_bmin",
                "cleanbeam_bpa",
                "pss",
                "sbs",
            ]
        ]
    )
