"""
Unit tests for the ska_ost_senscalc.utilities module.
"""

import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import SkyCoord

import ska_ost_senscalc.utilities as utils
from ska_ost_senscalc.utilities import (
    Atmosphere,
    Celestial,
    DishType,
    TelParams,
    Utilities,
)

TARGET = SkyCoord(10.625, 41.2, frame="icrs", unit="deg")


def test_Tcmb():  # pylint: disable=invalid-name
    """
    Verify that the Tcmb is 2.73K
    """
    assert Celestial.Tcmb.to_value(u.K) == pytest.approx(2.73)


def test_Tgal():  # pylint: disable=invalid-name
    """
    Verify that the Tcmb is 2.73K"""

    # print(
    #    "Tgal 1", Celestial().calculate_Tgal(TARGET, np.array([5.0]) * u.GHz, DishType.SKA1, 2.75)
    # )
    assert Celestial().calculate_Tgal(
        TARGET, np.array([5.0]) * u.GHz, DishType.SKA1, 2.75
    ).to_value(u.K) == pytest.approx(0.03343915)


def test_Tatm():  # pylint: disable=invalid-name
    """
    Verify Tatm calculation
    """

    # print('Tatm 1', Atmosphere().calculate_Tatm('Good', 23.0 * u.GHz))
    # print('Tatm 2', Atmosphere().calculate_Tatm('Average', 23.0 * u.GHz))
    # print('Tatm 3', Atmosphere().calculate_Tatm('Bad', 23.0 * u.GHz))
    assert Atmosphere().calculate_Tatm(5, 23.0 * u.GHz).to_value(
        u.K
    ) == pytest.approx(12.2347)
    assert Atmosphere().calculate_Tatm(10, 23.0 * u.GHz).to_value(
        u.K
    ) == pytest.approx(20.8967)
    assert Atmosphere().calculate_Tatm(20, 23.0 * u.GHz).to_value(
        u.K
    ) == pytest.approx(37.3897)


def test_get_tauz_atm():
    """
    Verify tauz lookup
    """

    # print('tauz 1', Atmosphere().get_tauz_atm('Good', 23.0 * u.GHz))
    # print('tauz 2', Atmosphere().get_tauz_atm('Average', 23.0 * u.GHz))
    # print('tauz 3', Atmosphere().get_tauz_atm('Bad', 23.0 * u.GHz))
    assert Atmosphere().get_tauz_atm(5, 23.0 * u.GHz) == pytest.approx(0.046)
    assert Atmosphere().get_tauz_atm(10, 23.0 * u.GHz) == pytest.approx(0.08)
    assert Atmosphere().get_tauz_atm(20, 23.0 * u.GHz) == pytest.approx(0.147)


def test_telparams_constants():
    """
    Verify constants
    """
    assert utils.SKA_DISH_DIAMETER.to_value(u.m) == pytest.approx(15.0)
    assert utils.SKA_NDISHES_MAX == 133
    assert utils.MEERKAT_DISH_DIAMETER.to_value(u.m) == pytest.approx(13.97)
    assert utils.MEERKAT_NDISHES_MAX == 64


def test_calculate_Tspl():  # pylint: disable=invalid-name
    """
    Verify TelParams.calculateTspl
    """
    telparams = TelParams()
    assert telparams.calculate_Tspl(DishType.SKA1).to_value(
        u.K
    ) == pytest.approx(3.0)
    assert telparams.calculate_Tspl(DishType.MeerKAT).to_value(
        u.K
    ) == pytest.approx(4.0)
    with pytest.raises(RuntimeError) as err:
        assert telparams.calculate_Tspl("other")
    assert str(err.value) == "bad dish_type: other"


def test_calculate_Trcv():  # pylint: disable=invalid-name
    """
    Verify TelParams.calculate_Trcv
    """
    telparams = TelParams()
    # print('Trcv 1', telparams.calculate_Trcv(0.5 * u.GHz, 'Band 1', DishType.SKA1))
    # print('Trcv 2', telparams.calculate_Trcv(3.0 * u.GHz, 'Band 3', DishType.SKA1))
    # print('Trcv 3', telparams.calculate_Trcv(15.0 * u.GHz, 'Band 5a', DishType.SKA1))
    # print('Trcv 4', telparams.calculate_Trcv(0.5 * u.GHz, '', DishType.MeerKAT))
    # print('Trcv 5', telparams.calculate_Trcv(1.5 * u.GHz, '', DishType.MeerKAT))
    # print('Trcv 6', telparams.calculate_Trcv(2.0 * u.GHz, '', DishType.MeerKAT))
    assert telparams.calculate_Trcv(
        0.5 * u.GHz, "Band 1", DishType.SKA1
    ).to_value(u.K) == pytest.approx(16.875)
    assert telparams.calculate_Trcv(
        3.0 * u.GHz, "Band 3", DishType.SKA1
    ).to_value(u.K) == pytest.approx(7.5)
    assert telparams.calculate_Trcv(
        15.0 * u.GHz, "Band 5a", DishType.SKA1
    ).to_value(u.K) == pytest.approx(14.75)
    assert telparams.calculate_Trcv(
        0.5 * u.GHz, "", DishType.MeerKAT
    ).to_value(u.K) == pytest.approx(11.36)
    assert telparams.calculate_Trcv(
        1.5 * u.GHz, "", DishType.MeerKAT
    ).to_value(u.K) == pytest.approx(7.895044301313156)
    assert telparams.calculate_Trcv(
        2.0 * u.GHz, "", DishType.MeerKAT
    ).to_value(u.K) == pytest.approx(7.5)
    with pytest.raises(RuntimeError) as err:
        assert telparams.calculate_Trcv(0.5 * u.GHz, "Band 1", "other")
    assert str(err.value) == "bad dish_type: other"


def test_dish_area():
    """
    Verify TelParams.dish_area
    """
    telparams = TelParams()
    # print('dish_area 1', telparams.dish_area(DishType.SKA1))
    # print('dish_area 2', telparams.dish_area(DishType.MeerKAT))
    assert telparams.dish_area(DishType.SKA1).to_value(
        u.m * u.m
    ) == pytest.approx(176.71458676442587)
    assert telparams.dish_area(DishType.MeerKAT).to_value(
        u.m * u.m
    ) == pytest.approx(153.2790124269931)
    with pytest.raises(RuntimeError) as err:
        assert telparams.dish_area("other")
    assert str(err.value) == "bad dish_type: other"


def test_dish_efficiency():
    """
    Verify TelParams.calculate_dish_efficiency
    """
    telparams = TelParams()
    # print('dish_efficiency 1', telparams.calculate_dish_efficiency(2.0 * u.GHz, DishType.SKA1))
    # print('dish_efficiency 2', telparams.calculate_dish_efficiency(1.5 * u.GHz, DishType.MeerKAT))
    assert telparams.calculate_dish_efficiency(
        2.0 * u.GHz, DishType.SKA1
    ) == pytest.approx(0.8892369467101581)
    assert telparams.calculate_dish_efficiency(
        1.5 * u.GHz, DishType.MeerKAT
    ) == pytest.approx(0.7649895704337154)
    with pytest.raises(RuntimeError) as err:
        assert telparams.calculate_dish_efficiency(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_dish_fwhm():
    """
    Verify TelParams.dish_fwhm
    """
    # print('dish_fwhm 1', TelParams.dish_fwhm(2.0 * u.GHz, DishType.SKA1).to_value(u.deg))
    # print('dish_fwhm 2', TelParams.dish_fwhm(1.5 * u.GHz, DishType.MeerKAT).to_value(u.deg))
    assert TelParams.dish_fwhm(2.0 * u.GHz, DishType.SKA1).to_value(
        u.deg
    ) == pytest.approx(0.6595434076000001)
    assert TelParams.dish_fwhm(1.5 * u.GHz, DishType.MeerKAT).to_value(
        u.deg
    ) == pytest.approx(0.9442282141732283)
    with pytest.raises(RuntimeError) as err:
        assert TelParams.dish_fwhm(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_mid_core_location():
    """
    Verify TelParams.mid_core_location
    """
    # print("mid_core_location", TelParams.mid_core_location().lon.to_value(u.deg))
    # print("mid_core_location", TelParams.mid_core_location().lat.to_value(u.deg))
    # print("mid_core_location", TelParams.mid_core_location().height.to_value(u.m))
    assert TelParams.mid_core_location().lon.to_value(u.deg) == pytest.approx(
        21.411127777777775
    )
    assert TelParams.mid_core_location().lat.to_value(u.deg) == pytest.approx(
        -30.72113
    )
    assert TelParams.mid_core_location().height.to_value(u.m) == pytest.approx(
        1000.0
    )


def test_sensible_units():
    """
    Verify that the conversion to sensible units works
    """
    # print('test_sensible_units -10', Utilities.sensible_units(3e-10 * u.Jy))
    # print('test_sensible_units -9', Utilities.sensible_units(3e-9 * u.Jy))
    # print('test_sensible_units -8', Utilities.sensible_units(3e-8 * u.Jy))
    # print('test_sensible_units -7', Utilities.sensible_units(3e-7 * u.Jy))
    # print('test_sensible_units -6', Utilities.sensible_units(3e-6 * u.Jy))
    # print('test_sensible_units -5', Utilities.sensible_units(3e-5 * u.Jy))
    # print('test_sensible_units -4', Utilities.sensible_units(3e-4 * u.Jy))
    # print('test_sensible_units -3', Utilities.sensible_units(3e-3 * u.Jy))
    # print('test_sensible_units -2', Utilities.sensible_units(3e-2 * u.Jy))
    # print('test_sensible_units -1', Utilities.sensible_units(3e-1 * u.Jy))
    # print('test_sensible_units 0', Utilities.sensible_units(3 * u.Jy))
    # print('test_sensible_units 1', Utilities.sensible_units(3e1 * u.Jy))
    # print('test_sensible_units 2', Utilities.sensible_units(3e2 * u.Jy))
    # print('test_sensible_units 3', Utilities.sensible_units(3e3 * u.Jy))
    # print('test_sensible_units 4', Utilities.sensible_units(3e4 * u.Jy))
    # print('test_sensible_units 5', Utilities.sensible_units(3e5 * u.Jy))
    # print('test_sensible_units 6', Utilities.sensible_units(3e6 * u.Jy))
    # print('test_sensible_units 7', Utilities.sensible_units(3e7 * u.Jy))
    # print('test_sensible_units 8', Utilities.sensible_units(3e8 * u.Jy))

    assert Utilities.sensible_units(3e-10 * u.Jy).unit == u.nJy
    assert Utilities.sensible_units(3e-9 * u.Jy).unit == u.nJy
    assert Utilities.sensible_units(3e-8 * u.Jy).unit == u.nJy
    assert Utilities.sensible_units(3e-7 * u.Jy).unit == u.uJy
    assert Utilities.sensible_units(3e-6 * u.Jy).unit == u.uJy
    assert Utilities.sensible_units(3e-5 * u.Jy).unit == u.uJy
    assert Utilities.sensible_units(3e-4 * u.Jy).unit == u.mJy
    assert Utilities.sensible_units(3e-3 * u.Jy).unit == u.mJy
    assert Utilities.sensible_units(3e-2 * u.Jy).unit == u.mJy
    assert Utilities.sensible_units(3e-1 * u.Jy).unit == u.Jy
    assert Utilities.sensible_units(3 * u.Jy).unit == u.Jy
    assert Utilities.sensible_units(3e1 * u.Jy).unit == u.Jy
    assert Utilities.sensible_units(3e2 * u.Jy).unit == u.kJy
    assert Utilities.sensible_units(3e3 * u.Jy).unit == u.kJy
    assert Utilities.sensible_units(3e4 * u.Jy).unit == u.kJy
    assert Utilities.sensible_units(3e5 * u.Jy).unit == u.MJy
    assert Utilities.sensible_units(3e6 * u.Jy).unit == u.MJy
    assert Utilities.sensible_units(3e7 * u.Jy).unit == u.MJy
    assert Utilities.sensible_units(3e8 * u.Jy).unit == u.MJy


def test_to_astropy():
    """Verify that to_astropy works correctly and fails as expected"""
    assert Utilities.to_astropy(1, u.s) == 1 * u.s
    assert Utilities.to_astropy(1, u.Jy) == 1 * u.Jy
    assert Utilities.to_astropy(10 * u.s, u.s) == 10 * u.s
    assert Utilities.to_astropy(1 * u.mJy, u.Jy) == 0.001 * u.Jy

    assert np.all(
        Utilities.to_astropy(np.array([1, 10]), u.s) == np.array([1, 10]) * u.s
    )
    assert np.all(
        Utilities.to_astropy(np.array([1, 10]), u.Jy)
        == np.array([1, 10]) * u.Jy
    )

    with pytest.raises(ValueError) as err:
        assert Utilities.to_astropy(1, u.GHz)
    assert "The unit must be one of the following default units: " in str(
        err.value
    )

    with pytest.raises(ValueError) as err:
        assert Utilities.to_astropy(1 * u.s, u.Hz)
    assert (
        str(err.value) == "'s' (time) and 'Hz' (frequency) are not convertible"
    )


def test_to_astropy_when_input_is_None():
    assert Utilities.to_astropy(None, u.s) is None


def test_Tx():  # pylint: disable=invalid-name
    """
    Verify that the correction applied to temperature is calculated correctly
    """

    # print('test_Tx', Utiities.Tx(1.0e9 * u.Hz, 10.0 * u.K))

    assert Utilities.Tx(1.0e9 * u.Hz, 10.0 * u.K).to_value(
        u.K
    ) == pytest.approx(9.976022)
