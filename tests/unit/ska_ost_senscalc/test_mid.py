"""
Unit tests for the ska_ost_senscalc.mid module.
"""
import astropy.units as u
import pytest
from astropy.coordinates import SkyCoord

import ska_ost_senscalc.mid as mid

NEVER_UP = SkyCoord(0.0 * u.deg, 70.0 * u.deg, frame="icrs")


def test_SEFD_antenna():  # pylint: disable=invalid-name
    """Verify performance of SEFD_antenna."""
    # print('sefd antenna', mid.SEFD_antenna(1 * u.K, 100 * u.m * u.m))

    assert mid.SEFD_antenna(1 * u.K, 100 * u.m * u.m).to_value(
        "J / m2"
    ) == pytest.approx(2.761298e-25)


def test_SEFD_array():  # pylint: disable=invalid-name
    """Verify performance of SEFD_antenna."""
    # print(
    #    "sefd array",
    #    mid.SEFD_array(100, 50, 2.0 * u.Unit("J / m2"), 1.0 * u.Unit("J / m2")),
    # )

    assert mid.SEFD_array(
        100, 50, 2.0 * u.Unit("J / m2"), 1.0 * u.Unit("J / m2")
    ).to_value("J / m2") == pytest.approx(0.01003771)
