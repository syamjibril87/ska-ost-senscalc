import astropy.units as u
import pytest
from astropy.coordinates import SkyCoord
from dictdiffer import diff
from numpy import array

import ska_ost_senscalc.mid_prop as mid

TARGET = SkyCoord(359.94423568 * u.deg, -00.04616002 * u.deg, frame="galactic")
NEVER_UP = SkyCoord(0.0 * u.deg, 70.0 * u.deg, frame="icrs")

# Defining some basic test cases.

# For historical reasons (changing the tests to follow the code developemnt),
# Tgal is sometimes overriden and sometimes left as the default.


ETA_SYSTEM = 1.0
TGAL = u.K * 17.1 * (0.408 / 0.5) ** 2.75
TESTCASE1 = mid.Calculator(
    rx_band="Band 1",
    frequency=0.5 * u.GHz,
    target=TARGET,
    bandwidth=0.2 * u.GHz,
    array_configuration="SKA1 (133 x 15m)",
    pwv=20,
    el=90 * u.deg,
    eta_system=ETA_SYSTEM,
    t_gal_meer=TGAL,
    t_gal_ska=TGAL,
)
TESTCASE2 = mid.Calculator(
    rx_band="Band 2",
    frequency=1.3 * u.GHz,
    target=TARGET,
    bandwidth=0.4 * u.GHz,
    array_configuration="meerKAT",
    pwv=5,
    el=90 * u.deg,
    eta_system=ETA_SYSTEM,
)
TGAL = u.K * 17.1 * (0.408 / 2.0) ** 2.75
TESTCASE3 = mid.Calculator(
    rx_band="Band 3",
    frequency=2.0 * u.GHz,
    target=TARGET,
    bandwidth=0.8 * u.GHz,
    array_configuration="full",
    pwv=10,
    el=90 * u.deg,
    eta_system=ETA_SYSTEM,
    t_gal_meer=TGAL,
    t_gal_ska=TGAL,
)
TESTCASE4 = mid.Calculator(
    rx_band="Band 4",
    frequency=4.0 * u.GHz,
    target=TARGET,
    bandwidth=0.4 * u.GHz,
    array_configuration="meerKAT",
    el=90 * u.deg,
    pwv=5,
    eta_system=ETA_SYSTEM,
)
TGAL = u.K * 17.1 * (0.408 / 6.0) ** 2.75
TESTCASE5 = mid.Calculator(
    rx_band="Band 5a",
    frequency=6.0 * u.GHz,
    target=TARGET,
    bandwidth=0.4 * u.GHz,
    array_configuration="meerKAT",
    pwv=5,
    el=90 * u.deg,
    eta_system=ETA_SYSTEM,
    t_gal_meer=TGAL,
    t_gal_ska=TGAL,
)
TESTCASE6 = mid.Calculator(
    rx_band="Band 5b",
    frequency=13.0 * u.GHz,
    target=TARGET,
    bandwidth=1.0 * u.GHz,
    array_configuration="full",
    pwv=10,
    el=90 * u.deg,
    eta_system=ETA_SYSTEM,
)

TARGET2 = SkyCoord("13h25m27.6s", "-43d01m09.00s", frame="icrs")
TESTCASE7 = mid.Calculator(
    rx_band="Band 1",
    frequency=0.7 * u.GHz,
    target=TARGET2,
    bandwidth=0.3 * u.GHz,
    array_configuration="full",
    pwv=5,
    el=90 * u.deg,
    eta_pointing=0.5,
    eta_coherence=0.5,
    eta_digitisation=0.5,
    eta_correlation=0.5,
    eta_bandpass=0.5,
    n_ska=100,
    eta_ska=0.7,
    t_spl_ska=10.0 * u.K,
    t_rx_ska=20.0 * u.K,
    n_meer=30,
    eta_meer=0.8,
    t_spl_meer=15.0 * u.K,
    t_rx_meer=25.0 * u.K,
    t_sky_ska=15.0 * u.K,
    t_sky_meer=15.0 * u.K,
    t_gal_ska=u.K * 17.1 * (0.408 / 0.7) ** 2.75,
    t_gal_meer=u.K * 17.1 * (0.408 / 0.7) ** 2.75,
)
# zoom
TESTCASE8 = mid.Calculator(
    rx_band="Band 1",
    frequency=0.7 * u.GHz,
    target=TARGET2,
    bandwidth=0.21 * u.kHz,
    array_configuration="full",
    pwv=5,
    el=90 * u.deg,
    eta_pointing=0.5,
    eta_coherence=0.5,
    eta_digitisation=0.5,
    eta_correlation=0.5,
    eta_bandpass=0.5,
    n_ska=100,
    eta_ska=0.7,
    t_spl_ska=10.0 * u.K,
    t_rx_ska=20.0 * u.K,
    n_meer=30,
    eta_meer=0.8,
    t_spl_meer=15.0 * u.K,
    t_rx_meer=25.0 * u.K,
    t_sky_ska=15.0 * u.K,
    t_sky_meer=15.0 * u.K,
    t_gal_ska=u.K * 17.1 * (0.408 / 0.7) ** 2.75,
    t_gal_meer=u.K * 17.1 * (0.408 / 0.7) ** 2.75,
)


def test_calculate_sensitivity():
    """
    Verify the sensitivity of a proposed observation for a given integration
    """

    # print("test sens1", TESTCASE1.calculate_sensitivity(3600 * u.s).to_value(u.Jy))
    # print("test sens2", TESTCASE2.calculate_sensitivity(14400 * u.s).to_value(u.Jy))
    # print("test sens3", TESTCASE3.calculate_sensitivity(3600 * u.s).to_value(u.Jy))
    # print("test sens4", TESTCASE4.calculate_sensitivity(14400 * u.s).to_value(u.Jy))
    # print("test sens5", TESTCASE5.calculate_sensitivity(3600 * u.s).to_value(u.Jy))
    # print("test sens6", TESTCASE6.calculate_sensitivity(14400 * u.s).to_value(u.Jy))
    # print("test sens7", TESTCASE7.calculate_sensitivity(10 * u.s).to_value(u.Jy))
    # print("test sens8", TESTCASE8.calculate_sensitivity(10 * u.s).to_value(u.Jy))

    assert TESTCASE1.calculate_sensitivity(3600 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(8.3811e-06, abs=1e-9)
    assert TESTCASE2.calculate_sensitivity(14400 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(7.5100e-06, abs=1e-9)
    assert TESTCASE3.calculate_sensitivity(3600 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(6.983e-07, abs=1e-9)
    assert TESTCASE4.calculate_sensitivity(14400 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(2.1025e-06, abs=1e-9)
    assert TESTCASE5.calculate_sensitivity(3600 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(3.5580e-06, abs=1e-9)
    assert TESTCASE6.calculate_sensitivity(14400 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(4.51583e-07, abs=1e-9)
    assert TESTCASE7.calculate_sensitivity(10 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(0.00336668337, abs=1e-5)
    assert TESTCASE8.calculate_sensitivity(10 * u.s)[0].to_value(
        u.Jy
    ) == pytest.approx(4.02348, abs=1e-3)


def test_calculate_sensitivity_negative_input():
    with pytest.raises(RuntimeError) as err:
        assert TESTCASE1.calculate_sensitivity(-1 * u.s)
    assert str(err.value) == "negative integration time"


def test_calculate_integration_time():
    """
    Verify the integration time of a proposed observation for a given sensitivity
    """

    # print("test int 1", TESTCASE1.calculate_integration_time(1 * u.uJy))
    # print("test int 2", TESTCASE2.calculate_integration_time(0.1 * u.uJy))
    # print("test int 3", TESTCASE3.calculate_integration_time(1 * u.uJy))
    # print("test int 4", TESTCASE4.calculate_integration_time(0.1 * u.uJy))
    # print("test int 5", TESTCASE5.calculate_integration_time(1 * u.uJy))
    # print("test int 6", TESTCASE6.calculate_integration_time(0.1 * u.uJy))

    assert TESTCASE1.calculate_integration_time(1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(252877, abs=1)
    assert TESTCASE2.calculate_integration_time(0.1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(81218186, abs=1)
    assert TESTCASE3.calculate_integration_time(1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(1755, abs=1)
    assert TESTCASE4.calculate_integration_time(0.1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(6365825, abs=1)
    assert TESTCASE5.calculate_integration_time(1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(45573, abs=1)
    assert TESTCASE6.calculate_integration_time(0.1 * u.uJy)[0].to_value(
        u.s
    ) == pytest.approx(293697, abs=1)


def test_calculate_integration_time_negative_sensitivity():
    with pytest.raises(RuntimeError) as err:
        assert TESTCASE1.calculate_integration_time(-1.0 * u.uJy)
    assert str(err.value) == "negative sensitivity"
