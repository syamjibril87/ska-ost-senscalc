"""
Unit tests for the ska_ost_senscalc.subarray module.
"""

import os
from pathlib import Path

import pytest

from ska_ost_senscalc.subarray import Subarray, SubarraySchema, SubarrayStorage
from ska_ost_senscalc.utilities import MEERKAT_NDISHES_MAX, SKA_NDISHES_MAX

BASE_DIR = Path(__file__).resolve().parents[3]
TEST_STORAGE_PATH = Path(
    BASE_DIR, "src", "ska_ost_senscalc", "static", "subarrays"
)

# tell black to not format the following dicts
# fmt: off
subarray_full_data = {
    "name": "full",
    "configuration": str(TEST_STORAGE_PATH / "ska1mid.cfg"),
    "ids": [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
        37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
        54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
        71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87,
        88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103,
        104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117,
        118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131,
        132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145,
        146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
        160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173,
        174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187,
        188, 189, 190, 191, 192, 193, 194, 195, 196,
    ],
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e",
}
subarray_extended_data = {
    "name": "SKA1 (133 x 15m)",
    "configuration": str(TEST_STORAGE_PATH / "ska1mid.cfg"),
    "ids": [
        64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
        81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97,
        98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
        112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125,
        126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
        140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153,
        154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167,
        168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181,
        182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195,
        196,
    ],
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e",
}
subarray_core_data = {
    "name": "meerKAT",
    "configuration": str(TEST_STORAGE_PATH / "ska1mid.cfg"),
    "ids": [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
        37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
        54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
    ],
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e",
}
subarray_bad_checksum_data = {
    "name": "meerKAT",
    "configuration": str(TEST_STORAGE_PATH / "ska1mid.cfg"),
    "ids": [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
        37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
        54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
    ],
    "md5_checksum": "bad??checksum",
}
# fmt: on

subarray_full = Subarray(**subarray_full_data)
subarray_extended = Subarray(**subarray_extended_data)
subarray_core = Subarray(**subarray_core_data)


def test_local_storage_listing():
    """Verify storage lists subarray files properly"""
    storage = SubarrayStorage(TEST_STORAGE_PATH)
    assert storage.list() == [
        "SKA1 (133 x 15m)",
        "custom",
        "full",
        "meerKAT",
    ]


def test_local_storage_load_data():
    """Verify that the storage instance load subarray files and transforms them to Subarray objects properly"""
    storage = SubarrayStorage(TEST_STORAGE_PATH)

    assert storage.load("meerKAT") == subarray_core
    assert storage.load("SKA1 (133 x 15m)") == subarray_extended
    assert storage.load("full") == subarray_full


def test_local_storage_load_non_existing_data():
    """Verify that an exception is raised when the storage file is not found"""
    storage = SubarrayStorage(TEST_STORAGE_PATH)
    with pytest.raises(ValueError):
        storage.load("aaa")


def test_subarray_class_properties():
    """Check that the two convenience properties return the correct number of antennas"""
    assert subarray_full.n_SKA == SKA_NDISHES_MAX
    assert subarray_full.n_MeerKAT == MEERKAT_NDISHES_MAX
    assert subarray_core.n_SKA == 0
    assert subarray_core.n_MeerKAT == MEERKAT_NDISHES_MAX
    assert subarray_extended.n_SKA == SKA_NDISHES_MAX
    assert subarray_extended.n_MeerKAT == 0


def test_subarray_marshmallow_deserialization():
    """Test that the marshmallow schema works: deserialization"""
    subarray_schema = SubarraySchema()
    assert subarray_full == subarray_schema.load(subarray_full_data)
    assert subarray_core == subarray_schema.load(subarray_core_data)
    assert subarray_extended == subarray_schema.load(subarray_extended_data)
    with pytest.raises(ValueError) as err:
        assert subarray_schema.load(subarray_bad_checksum_data)
    assert (
        str(err.value)
        == "Subarray checksum bad??checksum does not match configuration"
        " d41d8cd98f00b204e9800998ecf8427e"
    )


def test_subarray_marshmallow_serialization():
    """Test that the marshmallow schema works: serialization"""
    subarray_schema = SubarraySchema()
    comparison_data = dict(subarray_full_data)
    comparison_data["configuration"] = os.path.basename(
        comparison_data["configuration"]
    )
    assert comparison_data == subarray_schema.dump(subarray_full)
    comparison_data = dict(subarray_core_data)
    comparison_data["configuration"] = os.path.basename(
        comparison_data["configuration"]
    )
    assert comparison_data == subarray_schema.dump(subarray_core)
    comparison_data = dict(subarray_extended_data)
    comparison_data["configuration"] = os.path.basename(
        comparison_data["configuration"]
    )
    assert comparison_data == subarray_schema.dump(subarray_extended)
