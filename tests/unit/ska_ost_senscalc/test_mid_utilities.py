"""
Unit tests for the ska_ost_senscalc.mid_utilities module.
"""
import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import SkyCoord

import ska_ost_senscalc.mid_utilities as mid_utils
from ska_ost_senscalc.utilities import (
    Celestial,
    DishType,
    TelParams,
    Utilities,
)


def test_eta_bandpass():
    """Verify mid_utilities.eta_bandpass"""
    assert mid_utils.eta_bandpass() == 1.0


def test_eta_coherence():
    """Verify mid_utilities.eta_coherence"""
    # print("eta_coherence", mid_utils.eta_coherence(None, 8.0 * u.GHz))
    assert mid_utils.eta_coherence(8.0 * u.GHz) == pytest.approx(
        0.9945629318496559
    )


def test_eta_correlation():
    """Verify mid_utilities.eta_correlation"""
    assert mid_utils.eta_correlation() == pytest.approx(0.98)


def test_eta_digitisation():
    """
    Verify mid_utilities.BaseCalculator.eta_digitise
    """
    assert mid_utils.eta_digitisation("Band 1") == 0.999
    assert mid_utils.eta_digitisation("Band 2") == 0.999
    assert mid_utils.eta_digitisation("Band 3") == 0.998
    assert mid_utils.eta_digitisation("Band 5a") == 0.955
    assert mid_utils.eta_digitisation("Band 5b") == 0.955
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_digitisation("other")
    assert str(err.value) == "bad obs_band: other"


def test_eta_dish():
    """Validate mid_utilities.eta_dish"""
    assert mid_utils.eta_dish(
        0.5 * u.GHz, DishType.SKA1
    ) == TelParams.calculate_dish_efficiency(0.5 * u.GHz, DishType.SKA1)
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_dish(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_eta_point():
    """
    Verify mid_utilities.BaseCalculator.eta_point

    """
    # print("eta_point 1", mid_utils.eta_point(2.0 * u.GHz, DishType.SKA1))
    # print("eta_point 2", mid_utils.eta_point(1.5 * u.GHz, DishType.MeerKAT))
    assert mid_utils.eta_point(2.0 * u.GHz, DishType.SKA1) == pytest.approx(
        0.9999016485055535
    )
    assert mid_utils.eta_point(1.5 * u.GHz, DishType.MeerKAT) == pytest.approx(
        0.9999520116676085
    )
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_point(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_eta_rfi():
    """Validate mid_utilities.eta_rfi"""
    assert mid_utils.eta_rfi() == 1.00


def test_eta_system():
    """Validate mid_utilities.eta_system"""
    assert mid_utils.eta_system(0.5, 0.5, 0.5, 0.5, 0.5) == 0.5**5


def test_Tgal():
    """Validate mid_utilities.Tgal"""
    south_pole = SkyCoord(0.0, -90.0, unit="deg")
    assert mid_utils.Tgal(
        south_pole, np.array([0.5]) * u.GHz, DishType.SKA1, 2.75
    ) == Celestial().calculate_Tgal(
        south_pole, np.array([0.5]) * u.GHz, DishType.SKA1, 2.75
    )


def test_Trcv():
    """Validate mid_utilities.Trcv"""
    assert mid_utils.Trcv(
        0.5 * u.GHz, "Band 1", DishType.SKA1
    ) == TelParams.calculate_Trcv(0.5 * u.GHz, "Band 1", DishType.SKA1)


def test_Tsky():
    """Validate mid_utilities.Tsky"""
    location = TelParams.mid_core_location()
    south_pole = SkyCoord(0.0, -90.0, unit="deg")
    elevation = 90.0 * u.deg - (
        location.to_geodetic().lat - south_pole.icrs.dec
    )
    assert mid_utils.Tsky(5.0 * u.K, 0.5 * u.GHz, elevation, 10).to_value(
        u.K
    ) == pytest.approx(18.9435658)


def test_Tspl():
    """Validate mid_utilities.Tspl"""
    assert mid_utils.Tspl(DishType.SKA1) == TelParams.calculate_Tspl(
        DishType.SKA1
    )


def test_Tsys_dish():
    """Validate mid_utilities.Tsys_dish"""
    assert mid_utils.Tsys_dish(
        10.0 * u.K, 10.0 * u.K, 10.0 * u.K, 0.5 * u.GHz
    ) == Utilities.Tx(0.5 * u.GHz, 30.0 * u.K)


beam_params_missing = [
    (
        {
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "frequency",
    ),
    (
        {
            "zoom_frequencies": 13800000000.0 * u.Hz,
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.LINE,
        },
        "array_configuration",
    ),
    (
        {
            "zoom_frequencies": [500.0, 13800000000.0] * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.LINE,
        },
        "weighting",
    ),
    (
        {
            "frequency": 13800000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.ROBUST,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "robustness",
    ),
    (
        {
            "frequency": 13800000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "dec",
    ),
    (
        {
            "frequency": 13800000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
        },
        "calculator_mode",
    ),
    (
        {
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "frequency",
    ),
]


@pytest.mark.parametrize("params, missing_param", beam_params_missing)
def test_beam_parameters_unset_raise_runtime_errors(params, missing_param):
    try:
        _ = mid_utils.Beam(**params)

        # If test reaches this point, Beam class did not raise RuntimeError as expected
        assert False
    except RuntimeError as e:
        assert missing_param in str(e)


beam_params_invalid = [
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "SKA1",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "array configuration",
    ),
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.LINE,
        },
        "zoom_frequencies",
    ),
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.LINE,
        },
        "zoom_frequencies",
    ),
    (
        {
            "zoom_frequencies": [500, 1400],
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "frequency",
    ),
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 30.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
        },
        "taper",
    ),
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "SKA1 (133 x 15m)",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -60 * u.deg,
            "taper": 64.0 * u.arcsec,
            "calculator_mode": mid_utils.CalculatorMode.PULSAR,
        },
        "calculator mode",
    ),
]


@pytest.mark.parametrize("params, error_field", beam_params_invalid)
def test_invalid_options_raise_runtime_errors(params, error_field):
    try:
        _ = mid_utils.Beam(**params)

        # If test reaches this point, Beam class did not raise RuntimeError as expected
        assert False
    except RuntimeError as e:
        assert error_field in str(e)


def test_beam_class_does_not_require_taper_value():
    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "full",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -65 * u.deg,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    result = mid_utils.Beam(**params)
    assert result.taper == 0.0


def test_beam_class_converts_to_correct_units():
    """Test weighting_factor converts the input values to units
    expected by the lookup table.
    """
    params = {
        "frequency": 9.0 * u.GHz,
        "array_configuration": "full",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": -2.0,
        "dec": 10 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    result = mid_utils.Beam(**params)
    assert result.frequency == 9.0e9 * u.Hz


def test_beamsize_class_does_accepts_values():
    """
    test the BeamSize class can accept numbers without units
    """
    size = mid_utils.BeamSize(beam_maj=1.0, beam_min=2.0, beam_pa=3.0)

    assert size.beam_maj == 1.0 * u.deg
    assert size.beam_min == 2.0 * u.deg


def test_beamsize_class_can_convert_units():
    """
    test BeamSize class can accept units other than degrees
    """
    size = mid_utils.BeamSize(
        beam_maj=3600.0 * u.arcsec,
        beam_min=7200.0 * u.arcsec,
        beam_pa=0.03 * u.rad,
    )

    assert size.beam_maj == 1.0 * u.deg
    assert size.beam_min == 2.0 * u.deg
    assert size.beam_pa == 1.7188733853924696 * u.deg


beam_weighting_params_interpolated = [
    (
        {
            "frequency": 350000000.0 * u.Hz,
            "array_configuration": "full",
            "weighting": mid_utils.Weighting.UNIFORM,
            "dec": -65 * u.deg,
            "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
            "taper": 0.0,
        },
        4.97,
    ),
]


@pytest.mark.parametrize(
    "params, expected", beam_weighting_params_interpolated
)
def test_weighting_factor_interpolated(params, expected):
    """Validate beam weighting factor is correct using values not present in the
    beam_weighting_sensitivity.csv lookup table (interpolation required).

    Set accuracy to 1e-2 since not expecting exact values from interpolation.
    """
    result = mid_utils.Beam(**params).weighting_factor()
    assert result == pytest.approx(expected, rel=1e-2)


beam_weighting_params_direct = [
    {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "meerKAT",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 1.0,
        "dec": -90 * u.deg,
        "taper": 1280.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    },
    {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "SKA1 (133 x 15m)",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -50 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    },
    {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "full",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 2.0,
        "dec": 10 * u.deg,
        "taper": 2.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    },
]


def extract_reltonat(
    frequency,
    array_configuration,
    weighting,
    calculator_mode,
    taper,
    dec,
    robustness=0.0,
):
    """
    function to extract the beam_weighting value calculated by RASCIL/casa in the lookup table and comparing it to
    out method. Only works if the declination value is present in the look up table - i.e. no interpolation is
    required. By including a frequency parameter we can pass the same params as we do to the Beam class.
    """
    subarray = array_configuration + ".json"
    filtered_table = mid_utils.weighting_table[
        (
            (mid_utils.weighting_table["subarray"] == subarray)
            & (mid_utils.weighting_table["weighting"] == weighting.value)
            & (mid_utils.weighting_table["spec_mode"] == calculator_mode.value)
            & (mid_utils.weighting_table["taper_arcsec"] == taper.value)
        )
    ]
    if weighting == mid_utils.Weighting.ROBUST:
        filtered_table = filtered_table[
            (filtered_table["robustness"] == robustness)
        ]

    lookup_value = filtered_table["reltonat_casa"][
        (filtered_table["declination"] == dec.value)
    ]
    return lookup_value


@pytest.mark.parametrize("params", beam_weighting_params_direct)
def test_weighting_factor_direct(params):
    """Validate beam weighting factor is correct using values present in the
    beam_weighting_sensitivity.csv lookup table (no interpolation required).
    """
    beam = mid_utils.Beam(**params)
    result = beam.weighting_factor()
    assert result == pytest.approx(extract_reltonat(**params), rel=1e-6)


def test_weighting_factor_extrapolation_raises_runtime_error():
    """Test weighting_factor raises a value error if declination
    value given is outside the minimum and maximum values present
    in the lookup table.
    """
    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "meerKAT",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 2.0,
        "dec": 45 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    with pytest.raises(ValueError):
        _ = mid_utils.Beam(**params).weighting_factor()

    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "meerKAT",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 2.0,
        "dec": -91 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    with pytest.raises(ValueError):
        _ = mid_utils.Beam(**params).weighting_factor()


def test_beam_size_method_returns_expected_values_direct():
    """test that beam_size method returns a BeamSize object with expected values
    with no interpolation required. Note, this test will fail if the lookup table changes
    """
    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "meerKAT",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 1.0,
        "dec": -90 * u.deg,
        "taper": 0.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }
    beam = mid_utils.Beam(**params)
    size = beam.beam_size()
    cleanbeam_maj_from_lookup = 0.010760709788381763

    assert size[
        0
    ].beam_maj == cleanbeam_maj_from_lookup * u.deg * 1.4 * u.GHz / beam.frequency.to(
        u.GHz
    )


def test_beam_size_method_returns_expected_values_interpolation():
    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "meerKAT",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": 1.0,
        "dec": -60 * u.deg,
        "taper": 0.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }
    beam = mid_utils.Beam(**params)
    size = beam.beam_size()

    cleanbeam_maj_from_lookup_minus_50 = 0.008433896922575504
    cleanbeam_maj_from_lookup_minus_70 = 0.00893481759615964

    derived_value = size[0].beam_maj * beam.frequency.to(u.GHz) / 1.4 * u.GHz

    assert (
        cleanbeam_maj_from_lookup_minus_70
        > derived_value.value
        > cleanbeam_maj_from_lookup_minus_50
    )


def test_beam_size_method_produces_a_list_when_passed_multiple_frequencies():
    params = {
        "zoom_frequencies": [500, 1400, 15700] * u.MHz,
        "array_configuration": "SKA1 (133 x 15m)",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -70 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.LINE,
    }
    beam = mid_utils.Beam(**params)
    size = beam.beam_size()
    beam_min_from_lookup = 0.018012990115773408
    assert type(size) is list
    assert size[1].beam_min.value == beam_min_from_lookup


def test_surface_brightness_conversion_factor():
    """
    test case verified by MeerKAT sensitivity calculator
    """
    meerkat_test_case = mid_utils.Beam(
        frequency=1.4 * u.GHz,
        dec=-30,
        weighting=mid_utils.Weighting.ROBUST,
        robustness=0.0,
        array_configuration="meerKAT",
        calculator_mode=mid_utils.CalculatorMode.CONTINUUM,
    )

    assert meerkat_test_case.surface_brightness_conversion_factor()[
        0
    ].value == pytest.approx(1500, rel=100)


def test_surface_brightness_conversion_factor_calculatormode_line():
    params = {
        "zoom_frequencies": [500, 1400, 15700] * u.MHz,
        "array_configuration": "SKA1 (133 x 15m)",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -40 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.LINE,
    }
    beam = mid_utils.Beam(**params)
    sbcf = beam.surface_brightness_conversion_factor()
    assert sbcf[2] == 141.80886306240575 * u.K / u.Jy


def test_confusion_noise_returns_upperlimit():
    """
    test that the confusion noise method correctly returns value as an upper limit
    when the beam size is less than 3.817 arcsec.
    """
    params = {
        "frequency": 350000000.0 * u.Hz,
        "array_configuration": "full",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -65 * u.deg,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    beam = mid_utils.Beam(**params)
    beam_major_scaled = beam.beam_size()[0].beam_maj
    beam_major = beam_major_scaled * beam.frequency.to(u.GHz) / (1.4 * u.GHz)
    assert beam_major < 3.817 * u.arcsec
    assert beam.confusion_noise()[1][0] == mid_utils.Limit.UPPER.value


def test_confusion_noise_returns_correct_value():
    """
    A 10 arcsec beam at 1.4GHz should return a confusion noise of 3.68902241e-06
    """
    # identifying the row in the weighting table that has a beam major axis closest to 10 arcsec,
    # astropy tables does not support indexing as standard, so creating a test case manually

    # i = np.argsort(np.abs(mid_utils.weighting_table['cleanbeam_bmaj'] * u.deg - 10.0 * u.arcsec))

    params = {
        "frequency": 1.4 * u.GHz,
        "array_configuration": "full",
        "weighting": mid_utils.Weighting.ROBUST,
        "robustness": -1.0,
        "taper": 8.0 * u.arcsec,
        "dec": 10.0 * u.deg,
        "calculator_mode": mid_utils.CalculatorMode.CONTINUUM,
    }

    beam = mid_utils.Beam(**params)
    beam_major = beam.beam_size()[0].beam_maj.to(u.arcsec)

    assert beam_major.value == pytest.approx(10.0, rel=0.1)
    assert beam.confusion_noise()[0].value == pytest.approx(
        3.68902241e-06, rel=0.05
    )
    assert beam.confusion_noise()[1][0] == mid_utils.Limit.VALUE.value


def test_confusion_noise_calculatormode_line():
    params = {
        "zoom_frequencies": [500, 1400, 15700] * u.MHz,
        "array_configuration": "SKA1 (133 x 15m)",
        "weighting": mid_utils.Weighting.UNIFORM,
        "dec": -10 * u.deg,
        "taper": 64.0 * u.arcsec,
        "calculator_mode": mid_utils.CalculatorMode.LINE,
    }
    beam = mid_utils.Beam(**params)
    confusion_noise = beam.confusion_noise()
    assert len(confusion_noise[0]) is len(beam.zoom_frequencies)
    assert len(confusion_noise[1]) is len(beam.zoom_frequencies)
