import astropy.units as u
import numpy as np
import pytest
from astropy.coordinates import AltAz, SkyCoord

from ska_ost_senscalc.mid_prop import Calculator, clean_overrides
from ska_ost_senscalc.utilities import DishType, TelParams

target = SkyCoord(359.94423568 * u.deg, -00.04616002 * u.deg, frame="galactic")
tgal = u.K * 17.1 * (0.408 / 0.5) ** 2.75
TESTCASE1 = Calculator(
    rx_band="Band 1",
    frequency=0.5 * u.GHz,
    target=target,
    bandwidth=0.2 * u.GHz,
    array_configuration="SKA1 (133 x 15m)",
    pwv=20,
    el=90 * u.deg,
    eta_system=1.0,
)


def test_clean_overrides_one_flag():
    case = {"alpha_ska", "other"}
    assert clean_overrides(case) == {"other"}


def test_clean_overrides_two_flags():
    case = {"alpha_ska", "alpha_meer", "other"}
    assert clean_overrides(case) == {"alpha", "other"}


def test_clean_overrides_no_flags():
    case = {"other"}
    assert clean_overrides(case) == {"other"}


def test_setting_attribute_with_no_setter():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        bandwidth=0.2 * u.GHz,
        array_configuration="SKA1 (133 x 15m)",
        el=90 * u.deg,
    )
    with pytest.raises(AttributeError) as e:
        calc.frequency = 1.0 * u.GHz


def test_cascade_from_eta_point_to_eta_system():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        bandwidth=0.2 * u.GHz,
        array_configuration="SKA1 (133 x 15m)",
        el=90 * u.deg,
    )
    assert calc.eta_system == pytest.approx(0.9789931320953944)
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        bandwidth=0.2 * u.GHz,
        array_configuration="SKA1 (133 x 15m)",
        el=90 * u.deg,
        eta_pointing=0.5,
    )
    assert calc.eta_pointing == 0.5
    assert calc.eta_system < 0.9789931320953944


def test_override_of_eta_pointing_on_init():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        bandwidth=0.2 * u.GHz,
        array_configuration="SKA1 (133 x 15m)",
        el=90 * u.deg,
        pwv=20,
        eta_system=0.5,
    )
    assert "eta_pointing" in calc.overriden


def test_override_of_eta_pointing_after_init_fails():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        bandwidth=0.2 * u.GHz,
        array_configuration="SKA1 (133 x 15m)",
        el=90 * u.deg,
        pwv=20,
    )
    with pytest.raises(AttributeError) as e:
        calc.eta_system = 0.5


def test_override_n_antennas():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.n_ska == 133
    assert calc.n_meer == 64
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        n_ska=32,
    )
    assert calc.n_ska == 32
    assert "array_configuration" not in calc.overriden
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        n_ska=32,
        n_meer=20,
    )
    assert calc.n_ska == 32
    assert calc.n_ska == 32
    assert calc.n_meer == 20
    assert "array_configuration" in calc.overriden


# NOTE: These tests consider T_gal to be an array always
def test_t_gal_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.t_gal_ska[0].value == pytest.approx(395.3821529908282)
    assert calc.t_gal_meer[0].value == pytest.approx(374.6589205729342)


def test_override_t_gal():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
    )
    assert "alpha" not in calc.overriden
    assert calc.t_gal_ska[0].value == 300.0
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
        t_gal_meer=np.array([200.0]) * u.K,
    )
    assert calc.t_gal_meer[0].value == 200.0
    assert "alpha" in calc.overriden


def test_t_sky_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.t_sky_ska[0].value == pytest.approx(404.0202599679612)
    assert calc.t_sky_meer[0].value == pytest.approx(383.41392490804935)


def test_simple_cascade_of_overrides_from_t_sky():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_sky_ska=np.array([300.0]) * u.K,
    )
    assert "alpha" not in calc.overriden
    assert "t_gal_ska" in calc.overriden
    assert calc.t_sky_ska[0].value == 300.0
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_sky_ska=np.array([300.0]) * u.K,
        t_sky_meer=np.array([200.0]) * u.K,
    )
    assert "alpha" in calc.overriden
    assert "t_gal_meer" in calc.overriden
    assert calc.t_sky_meer[0].value == 200.0


def test_complex_cascade_of_overrides_from_t_sky():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
    )
    # Manually set Tgal
    assert "alpha" not in calc.overriden
    assert calc.t_gal_ska[0].value == 300.0
    assert calc.t_sky_ska[0].value == pytest.approx(309.17614667376097)
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
        t_gal_meer=np.array([200.0]) * u.K,
    )
    assert "alpha" in calc.overriden
    assert calc.t_gal_meer[0].value == 200.0
    assert calc.t_sky_meer[0].value == pytest.approx(209.7402351114373)
    # Manually set t_sky
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
        t_gal_meer=np.array([200.0]) * u.K,
        t_sky_ska=np.array([350.0]) * u.K,
    )
    assert "alpha" in calc.overriden
    assert "t_gal_ska" in calc.overriden
    assert calc.t_sky_ska[0].value == 350.0
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
        t_gal_ska=np.array([300.0]) * u.K,
        t_gal_meer=np.array([200.0]) * u.K,
        t_sky_ska=np.array([350.0]) * u.K,
        t_sky_meer=np.array([250.0]) * u.K,
    )
    assert "alpha" in calc.overriden
    assert "t_gal_meer" in calc.overriden
    assert calc.t_sky_meer[0].value == 250.0


def test_t_spl_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.t_spl_ska.value == pytest.approx(3.0)
    assert calc.t_spl_meer.value == pytest.approx(4.0)


def test_t_rx_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.t_rx_ska[0].value == pytest.approx(16.875)
    assert calc.t_rx_meer[0].value == pytest.approx(11.36)


def test_t_sys_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.t_sys_ska[0].value == pytest.approx(423.88326197299244)
    assert calc.t_sys_meer[0].value == pytest.approx(398.76192692133105)


def test_sefd_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.sefd_ska[0].value == pytest.approx(1.022529020598906e-22)
    assert calc.sefd_meer[0].value == pytest.approx(1.1090031168189728e-22)
    assert calc.sefd_array[0].value == pytest.approx(5.338992088808425e-25)


def test_tau_default_values():
    calc = Calculator(
        rx_band="Band 1",
        frequency=0.5 * u.GHz,
        target=target,
        array_configuration="full",
    )
    assert calc.tau[0].value == pytest.approx(0.00565685424949238)
