import pytest

from ska_ost_senscalc import create_app


@pytest.fixture
def client():
    """As explained in the Flask documentation: https://flask.palletsprojects.com/en/1.1.x/testing/"""
    connexion = create_app(low_enabled=True)
    connexion.app.config["TESTING"] = True
    with connexion.app.test_client() as client:
        yield client
