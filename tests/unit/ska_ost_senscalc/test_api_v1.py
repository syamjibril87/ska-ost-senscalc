"""
Unit tests for the ska_ost_senscalc.api_v1 module
At the moment, test the subarrays REST interface.
"""

from unittest import mock

from flask import json

from ska_ost_senscalc.api_v1_utilities import OPEN_API_V1


@mock.patch("ska_ost_senscalc.api_v1.get_calculate_response")
@mock.patch("ska_ost_senscalc.api_v1.parse_parameters_api_v1")
def test_calculate(mock_parse_fn, mock_calculate_fn, client):
    """Test calculate entry point."""
    expected_response_fields = OPEN_API_V1["paths"]["/calculate"]["get"][
        "responses"
    ]["200"]["content"]["application/json"]["schema"]["properties"].keys()

    mock_calculate_fn.return_value = {}
    rv = client.get("/api/v1/calculate")

    response = json.loads(rv.data)

    assert expected_response_fields == response.keys()
    assert "success" == response["status"]


def test_calculate_exception(client):
    """Test calculate entry point."""
    e_msg = "Mock Exception"
    expected_e_msg = (
        "Error parsing the parameters - RuntimeError('" + e_msg + "')"
    )
    expected_response_fields = OPEN_API_V1["paths"]["/calculate"]["get"][
        "responses"
    ]["400"]["content"]["application/json"]["schema"]["properties"].keys()

    with mock.patch(
        "ska_ost_senscalc.api_v1.parse_parameters_api_v1",
        side_effect=RuntimeError(e_msg),
    ):
        rv = client.get("/api/v1/calculate")
        response = json.loads(rv.data)

    assert expected_response_fields == response.keys()
    assert expected_e_msg == response["message"]
    assert "fail" == response["status"]


@mock.patch("ska_ost_senscalc.api_v1.get_weighting_response")
@mock.patch("ska_ost_senscalc.api_v1.parse_parameters_api_v1")
def test_weighting(mock_parse_fn, mock_weighting_fn, client):
    """Test calculate entry point."""
    expected_response_fields = OPEN_API_V1["paths"]["/weighting"]["get"][
        "responses"
    ]["200"]["content"]["application/json"]["schema"]["properties"].keys()

    mock_weighting_fn.return_value = {}
    rv = client.get("/api/v1/weighting")

    response = json.loads(rv.data)

    assert expected_response_fields == response.keys()
    assert "success" == response["status"]


def test_weighting_exception(client):
    """Test calculate entry point."""
    e_msg = "Mock Exception"
    expected_e_msg = (
        "Error parsing the parameters - RuntimeError('" + e_msg + "')"
    )
    expected_response_fields = OPEN_API_V1["paths"]["/weighting"]["get"][
        "responses"
    ]["400"]["content"]["application/json"]["schema"]["properties"].keys()

    with mock.patch(
        "ska_ost_senscalc.api_v1.parse_parameters_api_v1",
        side_effect=RuntimeError(e_msg),
    ):
        rv = client.get("/api/v1/weighting")
        response = json.loads(rv.data)

    assert expected_response_fields == response.keys()
    assert expected_e_msg == response["message"]
    assert "fail" == response["status"]


def test_subarrays_list(client):
    """Test the subarrays entry point."""
    rv = client.get("/api/v1/subarrays")
    data = json.loads(rv.data)
    # Check that the placeholder subarray entered in AT2-606 is removed when necessary
    assert [
        "SKA1 (133 x 15m)",
        "custom",
        "full",
        "meerKAT",
    ] == data
