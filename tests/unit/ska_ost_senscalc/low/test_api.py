from http import HTTPStatus
from unittest import mock

from flask import json

from ska_ost_senscalc.low.validation import DEFAULT_PARAMS


@mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults")
@mock.patch("ska_ost_senscalc.low.api.convert_input_and_calculate")
def test_calculate_with_no_params(mock_service_fn, mock_validate_fn, client):
    mock_validate_fn.return_value = DEFAULT_PARAMS
    mock_service_fn.return_value = {"sensitivity": 1234, "units": "uJy/beam"}
    response = client.get("/api/low/calculate")

    response_body = json.loads(response.data)

    assert response.status_code == HTTPStatus.OK
    assert response_body == {"sensitivity": 1234, "units": "uJy/beam"}

    mock_validate_fn.assert_called_with({})
    mock_service_fn.assert_called_with(DEFAULT_PARAMS)


@mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults")
@mock.patch("ska_ost_senscalc.low.api.convert_input_and_calculate")
def test_calculate_with_params(mock_service_fn, mock_validate_fn, client):
    params = {
        "num_stations": 40,
        "freq_centre": 250,
        "bandwidth": 50,
        "pointing_centre": "11:22:33 00:00:00",
        "duration": 3,
    }

    mock_validate_fn.return_value = params
    mock_service_fn.return_value = {"sensitivity": 1234, "units": "uJy/beam"}

    query_string = "&".join(
        [f"{key}={value}" for key, value in params.items()]
    )
    response = client.get(f"/api/low/calculate?{query_string}")

    response_body = json.loads(response.data)

    assert response.status_code == HTTPStatus.OK
    assert response_body == {"sensitivity": 1234, "units": "uJy/beam"}

    mock_validate_fn.assert_called_with(params)
    mock_service_fn.assert_called_with(params)


@mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults")
def test_calculate_handles_validation_error(mock_validate_fn, client):
    mock_validate_fn.side_effect = ValueError("test error")
    response = client.get("/api/low/calculate")

    response_body = json.loads(response.data)

    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response_body == {"detail": ["test error"]}


@mock.patch("ska_ost_senscalc.low.api.convert_input_and_calculate")
def test_calculate_handles_general_error(mock_service_fn, client):
    mock_service_fn.side_effect = RuntimeError()
    response = client.get("/api/low/calculate")

    response_body = json.loads(response.data)

    assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
    assert response_body == {"detail": "Internal Server Error"}
