import pytest

from ska_ost_senscalc.low.calculator import calculate_sensitivity
from ska_ost_senscalc.low.model import CalculatorInput, CalculatorResult


def test_calculator_duration_fits_in_visible_time():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre=200,
        bandwidth=300,
        pointing_centre="10:00:00 -30:00:00",
        duration=0.05,
    )

    assert calculate_sensitivity(calculator_input) == CalculatorResult(
        19.53744777171125, "uJy/beam"
    )


def test_calculator_duration_longer_than_visible_time():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre=200,
        bandwidth=300,
        pointing_centre="10:00:00 -30:00:00",
        duration=10,
    )

    assert calculate_sensitivity(calculator_input) == CalculatorResult(
        4.69275477961519, "uJy/beam"
    )


def test_calculator_source_always_up():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre=200,
        bandwidth=300,
        pointing_centre="00:00:00 -80:00:00",
        duration=0.05,
    )
    assert calculate_sensitivity(calculator_input) == CalculatorResult(
        93.62319017943254, "uJy/beam"
    )


def test_calculator_source_never_up_raises_error():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre=200,
        bandwidth=300,
        pointing_centre="00:00:00 80:00:00",
        duration=0.05,
    )
    with pytest.raises(ValueError) as err:
        calculate_sensitivity(calculator_input)

    assert (
        err.value.args[0]
        == "Specified pointing centre is always below the horizon from the SKA"
        " LOW site"
    )


def test_calculator_source_never_up_raises_error_for_low_elevation():
    """
    The sensitivities can't be calculated for low elevation, so we set the horizon of the observer
    object to be 15 degrees.
    """
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre=200,
        bandwidth=300,
        pointing_centre="08:00:00 50:00:00",
        duration=0.05,
    )
    with pytest.raises(ValueError) as err:
        calculate_sensitivity(calculator_input)

    assert (
        err.value.args[0]
        == "Specified pointing centre is always below the horizon from the SKA"
        " LOW site"
    )
