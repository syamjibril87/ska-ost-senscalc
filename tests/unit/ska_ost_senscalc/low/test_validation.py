import pytest

from ska_ost_senscalc.low.validation import validate_and_set_defaults


def test_defaults_are_set():

    user_input = {}

    expected_defaults = {
        "num_stations": 512,
        "freq_centre": 200,
        "bandwidth": 300,
        "pointing_centre": "10:00:00 -30:00:00",
        "duration": 1,
    }

    assert expected_defaults == validate_and_set_defaults(user_input)
    # Return a new dict rather than mutating
    assert {} == user_input


def test_spectral_window_within_limits():

    user_input = {"freq_centre": 100, "bandwidth": 50}

    expected_output = {
        "freq_centre": 100,
        "bandwidth": 50,
        "num_stations": 512,
        "pointing_centre": "10:00:00 -30:00:00",
        "duration": 1,
    }

    assert expected_output == validate_and_set_defaults(user_input)


def test_spectral_window_below_minimum():

    user_input = {"freq_centre": 100, "bandwidth": 150}

    with pytest.raises(ValueError) as err:
        validate_and_set_defaults(user_input)

    assert err.value.args == (
        "Spectral window defined by central frequency and bandwidth does not"
        " lie within the 50 - 350 MHz range.",
    )


def test_spectral_window_above_maximum():

    user_input = {"freq_centre": 300, "bandwidth": 150}

    with pytest.raises(ValueError) as err:
        validate_and_set_defaults(user_input)

    assert err.value.args == (
        "Spectral window defined by central frequency and bandwidth does not"
        " lie within the 50 - 350 MHz range.",
    )


def test_invalid_pointing():

    user_input = {"pointing_centre": "not a pointing"}

    with pytest.raises(ValueError) as err:
        validate_and_set_defaults(user_input)

    assert err.value.args == (
        "Specified pointing centre is invalid, expected format HH:MM:SS"
        " DD:MM:SS.",
    )
