from unittest import mock

import numpy as np

from ska_ost_senscalc.low.sefd_lookup import SEFDTable


@mock.patch("ska_ost_senscalc.low.sefd_lookup.sqlite3")
def test_sefd_lookup_sql_query_when_start_lst_greater(mocksql):
    """
    Test the inputs are correctly converted into the SQL query, particularly in the case where end_lst < start_lst.
    """

    # Must have more than k=3 datapoints for the cubic UnivariateSpline
    mock_execute = mock.MagicMock()
    mocksql.connect().__enter__().execute = mock_execute
    mock_execute.return_value.fetchall.return_value = [
        (49.92, 0.00649849),
        (58.88, 0.0083611),
        (69.12, 0.01071826),
        (79.36, 0.0137508),
    ]

    sefd_table = SEFDTable()

    sefd_table.lookup_stokes_i_sefd(10.2, 89, 5, 10, np.arange(0.0, 10.0))

    expected_query = (
        f"SELECT frequency_mhz,sensitivity "
        f"FROM Sensitivity "
        f"WHERE polarisation=:polarisation AND "
        f"ABS(za_deg-(90-:el))<=2.5 AND (lst>=:start_lst AND lst<=:end_lst)"
    )

    expected_params = {"el": 89, "end_lst": 10, "start_lst": 5}

    assert mock_execute.call_count == 2
    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "X"}
    )
    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "Y"}
    )


@mock.patch("ska_ost_senscalc.low.sefd_lookup.sqlite3")
def test_sefd_lookup_sql_query_when_end_lst_greater(mocksql):
    """
    Test the inputs are correctly converted into the SQL query, particularly in the case where end_lst >= start_lst.
    """

    # Must have more than k=3 datapoints for the cubic UnivariateSpline
    mock_execute = mock.MagicMock()
    mocksql.connect().__enter__().execute = mock_execute
    mock_execute.return_value.fetchall.return_value = [
        (49.92, 0.00649849),
        (58.88, 0.0083611),
        (69.12, 0.01071826),
        (79.36, 0.0137508),
    ]

    sefd_table = SEFDTable()

    sefd_table.lookup_stokes_i_sefd(359, 89, 10, 5, np.arange(0.0, 10.0))

    expected_query = (
        f"SELECT frequency_mhz,sensitivity FROM Sensitivity WHERE"
        f" polarisation=:polarisation AND"
        f" ABS(za_deg-(90-:el))<=2.5 AND"
        f" ((lst>=:start_lst AND lst<24.0) OR"
        f" (lst>=0 AND lst<=:end_lst))"
    )

    expected_params = {"el": 89, "end_lst": 5, "start_lst": 10}

    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "X"}
    )


@mock.patch("ska_ost_senscalc.low.sefd_lookup.sqlite3")
def test_sefd_lookup_sql_query_not_near_zenith_and_azim_clause(mocksql):
    """
    Test the inputs are correctly converted into the SQL query, particularly the coordainates.

    When the source is not within 2.5 degrees of the zenith (ie elevation > 87.5), the query should also match on the azim angle.
    The azim clause should match the nearest interval, except when >357.5 it should match 0 instead of 360
    """

    # Must have more than k=3 datapoints for the cubic UnivariateSpline
    mock_execute = mock.MagicMock()
    mocksql.connect().__enter__().execute = mock_execute
    mock_execute.return_value.fetchall.return_value = [
        (49.92, 0.00649849),
        (58.88, 0.0083611),
        (69.12, 0.01071826),
        (79.36, 0.0137508),
    ]

    sefd_table = SEFDTable()

    sefd_table.lookup_stokes_i_sefd(10.2, 41.3, 5, 10, np.arange(0.0, 10.0))

    expected_query = (
        "SELECT frequency_mhz,sensitivity "
        "FROM Sensitivity "
        "WHERE polarisation=:polarisation AND "
        "ABS(za_deg-(90-:el))<=2.5 AND ABS(azim_deg-:az)<=2.5 AND "
        "(lst>=:start_lst AND lst<=:end_lst)"
    )

    expected_params = {"az": 10.2, "el": 41.3, "end_lst": 10, "start_lst": 5}

    assert mock_execute.call_count == 2
    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "X"}
    )
    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "Y"}
    )

    # Now test query when end_lst >= start_lst and az > 357.5
    sefd_table.lookup_stokes_i_sefd(359, 41.3, 10, 5, np.arange(0.0, 10.0))

    expected_query = (
        f"SELECT frequency_mhz,sensitivity FROM Sensitivity WHERE"
        f" polarisation=:polarisation AND ABS(za_deg-(90-:el))<=2.5 AND"
        f" ABS(azim_deg+(360-:az))<=2.5 AND ((lst>=:start_lst AND lst<24.0) OR"
        f" (lst>=0 AND lst<=:end_lst))"
    )

    expected_params = {"az": 359, "el": 41.3, "end_lst": 5, "start_lst": 10}

    mock_execute.assert_any_call(
        expected_query, {**expected_params, "polarisation": "X"}
    )


@mock.patch("ska_ost_senscalc.low.sefd_lookup.sqlite3")
def test_sefd_lookup_same_dimensions_as_freq_fine_mhz(mocksql):
    """
    Test that mocked database rows are correctly turned into SEFD
    """

    # Must have more than k=3 datapoints for the cubic UnivariateSpline
    mocksql.connect().__enter__().execute().fetchall.return_value = [
        (10, 0.001),
        (20, 0.002),
        (30, 0.003),
        (40, 0.004),
    ]

    sefd_table = SEFDTable()

    freq_fine_mhz = np.arange(0.0, 10.0)
    result = sefd_table.lookup_stokes_i_sefd(1, 1, 1, 1, freq_fine_mhz)

    assert len(freq_fine_mhz) == len(result)

    freq_fine_mhz = np.arange(0.0, 1.0)
    result = sefd_table.lookup_stokes_i_sefd(1, 1, 1, 1, freq_fine_mhz)

    assert 4065863.9918226455 == result[0]
