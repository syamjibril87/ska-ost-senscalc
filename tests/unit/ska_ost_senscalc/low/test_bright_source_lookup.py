import unittest

import pytest

from ska_ost_senscalc.low.bright_source_lookup import BrightSourceCatalog


def test_default_catalog_is_valid():
    """
    Test if the default bright source catalog is valid.
    """
    mwa_cat = BrightSourceCatalog()
    catalog_length = mwa_cat._bright_source_coords.shape
    expected_length = (358,)
    assert catalog_length == expected_length


def test_threshold_catalog_is_valid():
    """
    Test if the bright source catalog generated with a flux
    density threshold of 100 Jy is valid.
    """
    mwa_cat = BrightSourceCatalog(threshold_jy=100.0)
    catalog_length = mwa_cat._bright_source_coords.shape
    expected_length = (11,)
    assert catalog_length == expected_length


def test_warning_raised_on_bright_source():
    """
    Test if the dynamic range warning is raised when the
    specified beam contains a bright source (Cen A in this case)
    """
    mwa_cat = BrightSourceCatalog()
    result = mwa_cat.check_for_bright_sources("13:25:28 -43:01:09", 150)
    assert result


def test_warning_not_raised_on_cold_sky():
    """
    Test if the dynamic range warning is not raised when the
    specified beam does not contain a bright source (CDFS in this case)
    """
    mwa_cat = BrightSourceCatalog()
    result = mwa_cat.check_for_bright_sources("03:30:00 -30:00:00", 150)
    assert not result
