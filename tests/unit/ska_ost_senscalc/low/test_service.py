from unittest import mock

from ska_ost_senscalc.low.model import CalculatorInput, CalculatorResult
from ska_ost_senscalc.low.service import convert_input_and_calculate


@mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
@mock.patch(
    "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
)
def test_calls_calculator_with_converted_input(
    mock_bright_src_check, mock_calculate_fn
):
    mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
    mock_bright_src_check.return_value = True

    user_input = {
        "num_stations": 42,
        "freq_centre": 400,
        "bandwidth": 125,
        "pointing_centre": "13:25:28 -43:01:09",
        "duration": 0.05,
    }

    expected_calculator_input = CalculatorInput(
        freq_centre=400,
        bandwidth=125,
        num_stations=42,
        pointing_centre="13:25:28 -43:01:09",
        duration=0.05,
    )

    assert convert_input_and_calculate(user_input) == {
        "sensitivity": 123,
        "units": "uJy/beam",
        "warn_msg": (
            "The specified pointing contains at least one source brighter than"
            " 10.0 Jy. Your observation may be dynamic range limited."
        ),
    }

    mock_calculate_fn.assert_called_once_with(expected_calculator_input)


@mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
@mock.patch(
    "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
)
def test_convert_input_and_calculate_no_bright_source(
    mock_bright_src_check, mock_calculate_fn
):
    """
    ska_ost_senscalc.low.service.convert_input_and_calculate() should not issue
    a warning message when the specified pointing does not contain a bright source.
    """
    mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
    mock_bright_src_check.return_value = False

    user_input = {
        "num_stations": 512,
        "freq_centre": 200,
        "bandwidth": 300,
        "pointing_centre": "03:30:00 -30:00:00",
        "duration": 10,
    }

    assert convert_input_and_calculate(user_input) == {
        "sensitivity": 123,
        "units": "uJy/beam",
    }
