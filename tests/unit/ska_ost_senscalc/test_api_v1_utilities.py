from inspect import signature
from unittest import mock

import astropy.units as u
import pytest
from astropy.coordinates import Angle

from ska_ost_senscalc.api_v1_utilities import (
    OPEN_API_V1,
    get_calculate_response,
    get_chunks,
    get_weighting_response,
    parse_parameters,
    parse_parameters_api_v1,
    to_float,
)
from ska_ost_senscalc.mid_utilities import (
    Beam,
    BeamSize,
    CalculatorMode,
    Limit,
    Weighting,
)


def test_to_float_none():
    assert to_float(None) is None


def test_to_float_empty():
    assert to_float("") is None


def test_to_float_float():
    assert to_float("2.0") == 2.0


def test_to_float_int():
    assert to_float("2", cast=int) == 2


def test_get_chunks():
    """This tests the get_chunks method for one set of inputs."""
    n_chunks = 3
    obs_freq = 0.7e9
    bandwidth = 0.4e9

    result = get_chunks(n_chunks, obs_freq, bandwidth)
    assert result == (
        [566666666.6666666, 700000000.0, 833333333.3333333],
        133333333.33333333,
    )


# Auxiliary classes to mock the Flask response
class MockArgList:
    def __init__(self, dict_args, dict_args_list=None):
        self._dict_args = dict_args
        if dict_args_list is not None:
            self._dict_args_list = dict_args_list

    def get(self, name):
        try:
            return self._dict_args[name]
        except KeyError:
            return None

    def getlist(self, name):
        try:
            return self._dict_args_list[name]
        except KeyError:
            return []


class MockRequest:
    """Class to mock the access to the arguments of a request"""

    def __init__(self, dict_args, dict_args_list=None):
        self.args = MockArgList(dict_args, dict_args_list=dict_args_list)


# Test the parse_parameter function
def test_parse_parameters_required_parameter_missing():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
    }
    # "bandwidth": 400000000  # not included
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    with pytest.raises(RuntimeError, match=r"^The parameter .* is required$"):
        _ = parse_parameters(request, params_definition=params_def)


def test_parse_parameters_array_parameter_parsed_as_list():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
    }
    dict_args_list = {"zoom_frequencies": [6600000000, 6700000000]}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    output = parse_parameters(request, params_definition=params_def)
    assert output["zoom_frequencies"] == [6600000000, 6700000000]


def test_parse_parameters_conversion_of_values():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "n_ska": 30,
    }
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    output = parse_parameters(request, params_definition=params_def)
    assert type(output["rx_band"]) == str
    assert type(output["frequency"]) == float
    assert type(output["n_ska"]) == int


def test_parse_parameters_parameter_out_of_range_maximum():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "n_ska": 230,
    }
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    with pytest.raises(ValueError):
        _ = parse_parameters(request, params_definition=params_def)


def test_parse_parameters_parameter_out_of_range_minimum():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "t_sky_ska": -1,
    }
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    with pytest.raises(ValueError):
        _ = parse_parameters(request, params_definition=params_def)


def test_parse_parameters_not_in_api_definition():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "n_total": 30,
    }
    # n_total is not in the API definition
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    output = parse_parameters(request, params_definition=params_def)
    assert "n_total" not in output.keys()


def test_parse_parameters_empty_input():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "alpha": "",
    }
    # alpha is empty but entered in the list
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    output = parse_parameters(request, params_definition=params_def)
    assert "alpha" not in output.keys()


def test_parse_parameters_parameter_0():
    dict_args = {
        "calculator_mode": "continuum",
        "array_configuration": "full",
        "frequency": 3000000000,
        "weighting": "robust",
        "robustness": 0,
        "ra_str": "13:25:27.60",
        "dec_str": "10:00:00.00",
        "el": 20,
        "integration_time": 3600,
    }
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/weighting"]["get"]["parameters"]
    output = parse_parameters(request, params_definition=params_def)
    assert output["robustness"] == 0.0


def test_parse_parameters_api_v1_equivalent_to_parse_parameters():
    dict_args = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
    }
    dict_args_list = {}
    request = MockRequest(dict_args, dict_args_list=dict_args_list)
    params_def = OPEN_API_V1["paths"]["/calculate"]["get"]["parameters"]
    output_parse_parameters = parse_parameters(
        request, params_definition=params_def
    )
    output_parse_parameters_api_v1 = parse_parameters_api_v1(request)
    assert output_parse_parameters == output_parse_parameters_api_v1


def test_get_calculate_response_sensitivity():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "integration_time": 600,
    }
    result = get_calculate_response(params)
    assert result["result"]["sensitivity"] > 0

    # line sensitivity should not be calculated if resolution not given
    assert "line_sensitivity" not in result["result"].keys()


def test_get_calculate_response_line_sensitivity():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "resolution": 210,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "integration_time": 600,
    }
    result = get_calculate_response(params)
    assert result["result"]["sensitivity"] > 0
    assert "line_sensitivity" in result["result"].keys()
    assert result["result"]["line_sensitivity"] > 0


def test_get_calculate_response_integration_time():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
    }
    result = get_calculate_response(params)
    assert result["result"]["integration_time"] > 0

    # line integration time should not be calculated if resolution not given
    assert "line_integration_time" not in result["result"].keys()


def test_get_calculate_response_line_integration_time():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "resolution": 210,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
    }
    result = get_calculate_response(params)
    assert result["result"]["integration_time"] > 0
    assert "line_integration_time" in result["result"].keys()
    assert result["result"]["line_integration_time"] > 0


def test_get_calculate_response_conflict_sensitivity_integration_time():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "integration_time": 1,
    }
    with pytest.raises(RuntimeError):
        _ = get_calculate_response(params)


def test_get_calculate_response_chunks():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "n_chunks": 3,
    }
    result = get_calculate_response(params)
    assert len(result["chunks"]) == 3


def test_get_calculate_response_zooms():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "zoom_frequencies": [6400000000, 6600000000],
        "zoom_resolutions": [1000, 1000],
        "zoom_sensitivities": [1.0, 0.001],
    }
    result = get_calculate_response(params)
    assert len(result["zooms"]) == 2


def test_get_calculate_response_zooms_missing_zoom_sensitivities():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "zoom_frequencies": [6400000000, 6600000000],
        "zoom_resolutions": [1000, 1000],
    }
    with pytest.raises(RuntimeError):
        _ = get_calculate_response(params)


def test_get_calculate_response_error_zooms_different_length():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "integration_time": 100,
        "zoom_frequencies": [6400000000, 6600000000],
        "zoom_resolutions": [1000, 1000, 1000],
    }
    with pytest.raises(RuntimeError):
        _ = get_calculate_response(params)


def test_get_calculate_response_error_zoom_frequencies_not_set():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "zoom_frequencies": [6400000000, 6600000000],
    }
    with pytest.raises(RuntimeError):
        _ = get_calculate_response(params)


def test_get_calculate_response_error_zoom_resolutions_not_set():
    params = {
        "frequency": 6500000000,
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "ra_str": "13:25:27.60",
        "dec_str": "-43:01:09.00",
        "sensitivity": 1,
        "zoom_resolutions": [64000, 6600],
    }
    with pytest.raises(RuntimeError):
        _ = get_calculate_response(params)


@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.confusion_noise")
@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.beam_size")
@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.weighting_factor")
@mock.patch(
    "ska_ost_senscalc.api_v1_utilities.Beam.surface_brightness_conversion_factor"
)
def test_get_weighting_response_normal_continuum_input(
    mock_sbsc_factor_fn,
    mock_weighting_factor_fn,
    mock_beam_size_fn,
    mock_confusion_noise_fn,
):
    mock_sbsc_factor_fn.return_value = 1 * u.K / u.Jy
    mock_weighting_factor_fn.return_value = 1

    mock_beam_size = [BeamSize(beam_pa=1, beam_maj=2, beam_min=3)]
    mock_beam_size_fn.return_value = mock_beam_size

    mock_confusion_noise_fn.return_value = ([1] * u.Jy, [Limit.VALUE.value])

    params = {
        "frequency": 6500000000,
        "array_configuration": "full",
        "weighting": "uniform",
        "dec_str": "10:00:00.00",
        "calculator_mode": "continuum",
        "taper": 4.0,
    }
    result = get_weighting_response(params)
    assert result["weighting_factor"] == 1
    assert result["sbs_conv_factor"] == 1
    assert (
        result["beam_size"][0]["beam_maj_scaled"]
        == mock_beam_size[0].beam_maj.value
    )
    assert (
        result["beam_size"][0]["beam_min_scaled"]
        == mock_beam_size[0].beam_min.value
    )
    assert result["beam_size"][0]["beam_pa"] == mock_beam_size[0].beam_pa.value
    assert result["confusion_noise"]["value"][0] == 1
    assert result["confusion_noise"]["limit_type"][0] == Limit.VALUE.value


@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.confusion_noise")
@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.beam_size")
@mock.patch("ska_ost_senscalc.api_v1_utilities.Beam.weighting_factor")
@mock.patch(
    "ska_ost_senscalc.api_v1_utilities.Beam.surface_brightness_conversion_factor"
)
def test_get_weighting_response_normal_zoom_frequencies(
    mock_sbsc_factor_fn,
    mock_weighting_factor_fn,
    mock_beam_size_fn,
    mock_confusion_noise_fn,
):
    mock_sbsc_factor_fn.return_value = [1, 2, 3] * u.K / u.Jy
    mock_weighting_factor_fn.return_value = 1

    mock_beam_size = [
        BeamSize(beam_pa=1, beam_maj=2, beam_min=3),
        BeamSize(beam_pa=4, beam_maj=5, beam_min=6),
        BeamSize(beam_pa=7, beam_maj=8, beam_min=9),
    ]
    mock_beam_size_fn.return_value = mock_beam_size

    mock_confusion_noise_fn.return_value = (
        [3, 2, 1] * u.Jy,
        [Limit.VALUE.value, Limit.UPPER.value, Limit.LOWER.value],
    )

    params = {
        "zoom_frequencies": [500000, 1400000000, 6500000000],
        "array_configuration": "full",
        "weighting": "uniform",
        "dec_str": "10:00:00.00",
        "calculator_mode": "line",
        "taper": 4.0,
    }
    result = get_weighting_response(params)
    assert result["weighting_factor"] == 1
    assert result["sbs_conv_factor"][1] == 2
    assert (
        result["beam_size"][2]["beam_maj_scaled"]
        == mock_beam_size[2].beam_maj.value
    )
    assert (
        result["beam_size"][1]["beam_min_scaled"]
        == mock_beam_size[1].beam_min.value
    )
    assert result["beam_size"][0]["beam_pa"] == mock_beam_size[0].beam_pa.value
    assert result["confusion_noise"]["value"][0] == 3
    assert result["confusion_noise"]["limit_type"][2] == Limit.LOWER.value


def test_weighting_response_returns_zero_and_limit_type_zero_for_upper_limit():
    params = {
        "zoom_frequencies": [500000, 1400000000, 6500000000],
        "rx_band": "Band 5a",
        "bandwidth": 400000000,
        "array_configuration": "full",
        "weighting": "uniform",
        "dec_str": "10:00:00.00",
        "calculator_mode": "line",
        "taper": 4.0,
    }

    result = get_weighting_response(params)

    kwargs_weighting = [
        p.name
        for p in signature(Beam).parameters.values()
        if p.kind == p.KEYWORD_ONLY
    ]

    a = Angle(params["dec_str"] + " degrees")
    params["dec"] = a.degree

    # Parse the weighting and calculator mode
    params["weighting"] = Weighting(params["weighting"])
    params["calculator_mode"] = CalculatorMode(params["calculator_mode"])
    # Keep only the params in the list of inputs
    relevant_params = {
        k: v for k, v in params.items() if k in kwargs_weighting
    }

    beam = Beam(**relevant_params)

    assert result["confusion_noise"]["value"][-1] == 0
    assert result["confusion_noise"]["limit_type"][-1] == Limit.VALUE.value
    assert beam.confusion_noise()[1][-1] == Limit.UPPER.value
