"""
Unit tests for the ska_ost_senscalc.api_v1 module
At the moment, test the subarrays REST interface.
"""
import copy

import pytest
from flask import json

from ska_ost_senscalc import create_app
from ska_ost_senscalc.api_v1_utilities import OPEN_API_V1


@pytest.fixture
def client():
    """As explained in the Flask documentation: https://flask.palletsprojects.com/en/1.1.x/testing/"""
    app = create_app(low_enabled=True).app
    app.config["TESTING"] = True
    with app.test_client() as client:
        yield client


CALCULATE_INPUT_PARAMS = {
    "frequency": 6500000000,
    "rx_band": "Band 5a",
    "bandwidth": 400000000,
    "ra_str": "13:25:27.60",
    "dec_str": "-43:01:09.00",
    "n_ska": 130,
    "integration_time": 1000,
}

CALCULATE_INPUT_PARAMS_SENSITIVITY = {
    "frequency": 6500000000,
    "rx_band": "Band 5a",
    "bandwidth": 400000000,
    "ra_str": "13:25:27.60",
    "dec_str": "-43:01:09.00",
    "n_ska": 130,
    "sensitivity": 1.5,
}

WEIGHTING_INPUT_PARAMS = [
    {
        "frequency": 6500000000,
        "array_configuration": "full",
        "weighting": "uniform",
        "dec_str": "10:00:00.00",
        "calculator_mode": "continuum",
        "taper": 4.0,
    },
    {
        "zoom_frequencies": [500000, 1400000000, 6500000000],
        "array_configuration": "full",
        "weighting": "uniform",
        "dec_str": "10:00:00.00",
        "calculator_mode": "line",
        "taper": 4.0,
    },
]


def get_response_fields(path: str, http_status: str):
    return OPEN_API_V1["paths"][path]["get"]["responses"][http_status][
        "content"
    ]["application/json"]["schema"]["properties"].keys()


def validate_response_value_types(response, result_obj):
    expected_fields = OPEN_API_V1["components"]["schemas"][result_obj][
        "properties"
    ]

    def get_type(value):
        if type(value).__name__ == "str":
            return "string"
        if type(value).__name__ == "float":
            return "number"
        if type(value).__name__ == "int":
            return "integer"
        if type(value).__name__ == "dict":
            return "object"
        if type(value).__name__ == "list":
            return "array"
        return type(value).__name__

    for response_field in response.keys():
        value_type = get_type(response[response_field])
        if expected_fields[response_field].get("type"):
            expected_type = expected_fields[response_field]["type"]
            assert value_type == expected_type, (
                f"Expected {response_field} to be {expected_type} but was"
                f" {value_type}"
            )


def test_calculate(client):
    """Test calculate entry point."""
    rv = client.get("/api/v1/calculate", query_string=CALCULATE_INPUT_PARAMS)

    response = json.loads(rv.data)

    assert get_response_fields("/calculate", "200") == response.keys()
    validate_response_value_types(
        response["data"]["result"], "CalculatorResult"
    )
    assert "success" == response["status"]


def test_calculate_given_sensitivity(client):
    """Test calculate entry point."""
    rv = client.get(
        "/api/v1/calculate", query_string=CALCULATE_INPUT_PARAMS_SENSITIVITY
    )

    response = json.loads(rv.data)

    assert get_response_fields("/calculate", "200") == response.keys()
    validate_response_value_types(
        response["data"]["result"], "CalculatorResult"
    )
    assert "success" == response["status"]


def test_calculate_exception(client):
    """Test calculate entry point."""
    error_params = copy.deepcopy(CALCULATE_INPUT_PARAMS)
    error_params["n_ska"] = 230  # Should not be more than 133
    expected_e_msg = "Error parsing the parameters"

    rv = client.get("/api/v1/calculate", query_string=error_params)
    response = json.loads(rv.data)

    assert get_response_fields("/calculate", "400") == response.keys()
    assert expected_e_msg in response["message"]
    assert "230" in response["message"]
    assert "fail" == response["status"]


@pytest.mark.parametrize("params", WEIGHTING_INPUT_PARAMS)
def test_weighting(client, params):
    """Test weighting entry point."""
    rv = client.get("/api/v1/weighting", query_string=params)

    response = json.loads(rv.data)

    assert get_response_fields("/weighting", "200") == response.keys()
    validate_response_value_types(response["data"], "WeightingResult")
    assert "success" == response["status"]


def test_weighting_exception(client):
    """Test weighting entry point."""
    error_params = copy.deepcopy(WEIGHTING_INPUT_PARAMS[0])
    error_params["weighting"] = "foo"
    expected_e_msg = "Error parsing the parameters"

    rv = client.get("/api/v1/weighting", query_string=error_params)
    response = json.loads(rv.data)

    assert get_response_fields("/weighting", "400") == response.keys()
    assert expected_e_msg in response["message"]
    assert "foo" in response["message"]
    assert "fail" == response["status"]


def test_subarrays_list(client):
    """Test the subarrays entry point."""
    rv = client.get("/api/v1/subarrays")
    data = json.loads(rv.data)
    # Check that the placeholder subarray entered in AT2-606 is removed when necessary
    assert [
        "SKA1 (133 x 15m)",
        "custom",
        "full",
        "meerKAT",
    ] == data
