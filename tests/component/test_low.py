from http import HTTPStatus
from json import loads
from os import getenv

import pytest
import requests

SENS_CALC_URL = getenv(
    "SENS_CALC_URL", "http://ska-ost-senscalc-rest-test:5000"
)
LOW_API_URL = f"{SENS_CALC_URL}/api/low"


def test_low_calculate_without_params():
    response = requests.get(f"{LOW_API_URL}/calculate")
    result = loads(response.content)

    assert response.status_code == HTTPStatus.OK
    assert result["sensitivity"] == 4.408735841136636
    assert result["units"] == "uJy/beam"


@pytest.mark.parametrize(
    "params, sensitivity_result",
    [
        (
            {
                "num_stations": 512,
                "freq_centre": 100,
                "bandwidth": 50,
                "pointing_centre": "08:00:00 20:00:50",
                "duration": 0.5,
            },
            26.38648336727443,
        ),
        (
            {
                "num_stations": 300,
                "freq_centre": 330,
                "bandwidth": 20,
                "pointing_centre": "00:00:00 00:00:00",
                "duration": 0.3,
            },
            60.949548954931885,
        ),
        (
            {
                "num_stations": 512,
                "freq_centre": 200,
                "bandwidth": 300,
                "pointing_centre": "10:00:00 -30:00:00",
                "duration": 0.05,
            },
            19.53744777171125,
        ),
        (
            {
                "num_stations": 512,
                "freq_centre": 200,
                "bandwidth": 300,
                "pointing_centre": "00:00:00 -29:00:00",
                "duration": 0.05,
            },
            18.963903533740122,
        ),
    ],
)
def test_low_calculate_with_params(params, sensitivity_result):
    query_string = "&".join(
        [f"{key}={value}" for key, value in params.items()]
    )
    response = requests.get(f"{LOW_API_URL}/calculate?{query_string}")
    result = loads(response.content)

    assert response.status_code == HTTPStatus.OK
    assert result["sensitivity"] == sensitivity_result
    assert result["units"] == "uJy/beam"
