"""
Unit tests for the generate_standard_subarrays script.
"""

import json
import os
import subprocess
from pathlib import Path

from scripts.generate_standard_subarrays import generate_subarrays

BASE_DIR = Path(__file__).resolve().parents[2]
TEST_STORAGE_PATH = Path(
    BASE_DIR, "src", "ska_ost_senscalc", "static", "subarrays"
)


def teardown_module():
    """Clean up JSON files generated during tests."""
    os.remove("meerKAT.json")
    os.remove("SKA1 (133 x 15m).json")
    os.remove("full.json")


def test_generate_standard_subarrays_script():
    """Verify that script generate_standard_subarrays works properly"""

    # run the script
    pipe = subprocess.Popen(
        "python {base_dir}/scripts/generate_standard_subarrays.py "
        "--configuration_path {config}".format(
            base_dir=BASE_DIR, config=(TEST_STORAGE_PATH / "ska1mid.cfg")
        ),
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    # check that the script has run with no screen output or error
    res = pipe.communicate()
    assert res == (b"", b"")

    # check that the generated json matches expected
    with open(TEST_STORAGE_PATH / "meerKAT.json", "r") as f:
        core_test = json.load(f)
    with open("meerKAT.json", "r") as f:
        core_local = json.load(f)
    assert core_test is not None and core_test == core_local

    with open(TEST_STORAGE_PATH / "SKA1 (133 x 15m).json", "r") as f:
        extended_test = json.load(f)
    with open("SKA1 (133 x 15m).json", "r") as f:
        extended_local = json.load(f)
    assert extended_test is not None and extended_test == extended_local

    with open(TEST_STORAGE_PATH / "full.json", "r") as f:
        full_test = json.load(f)
    with open("full.json", "r") as f:
        full_local = json.load(f)
    assert full_test is not None and full_test == full_local


def test_generate_subarrays():
    """Verify that generate_standard_subarrays method works properly"""

    generate_subarrays(configuration_path=TEST_STORAGE_PATH / "ska1mid.cfg")

    # check that the generated json matches expected
    with open(TEST_STORAGE_PATH / "meerKAT.json", "r") as f:
        core_test = json.load(f)
    with open("meerKAT.json", "r") as f:
        core_local = json.load(f)
    assert core_test is not None and core_test == core_local

    with open(TEST_STORAGE_PATH / "SKA1 (133 x 15m).json", "r") as f:
        extended_test = json.load(f)
    with open("SKA1 (133 x 15m).json", "r") as f:
        extended_local = json.load(f)
    assert extended_test is not None and extended_test == extended_local

    with open(TEST_STORAGE_PATH / "full.json", "r") as f:
        full_test = json.load(f)
    with open("full.json", "r") as f:
        full_local = json.load(f)
    assert full_test is not None and full_test == full_local
