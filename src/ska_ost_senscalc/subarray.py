"""Module to handle the subarray configurations"""

import fnmatch
import hashlib
import json
import logging
import os
from pathlib import Path

import numpy as np
from astropy.io import ascii
from marshmallow import Schema, fields, post_load

logger = logging.getLogger("senscalc")

STORAGE_PATH = Path(Path(__file__).resolve().parents[0], "static", "subarrays")


class Subarray:
    """
    Class to store the subarray data structure.

    :param name: name of the subarray
    :type name: str
    :param configuration: path to the configuration file
    :type configuration: str
    :param ids: list of selected antennas, specified by configuration file row number
    :type ids: list
    :param md5_checksum: checksum of configuration file to which ids refer
    :type md5_checksum: str

    The subarray definition lists selected antennas, identified by row numbers in the
    associated configuration file. Checksums are used to ensure that the subarray ids
    are appropriate to the given configuration file.

    There are two convenience properties, n_MeerKAT and n_SKA that return the number of
    antennas of each type.
    """

    def __init__(
        self, name: str, configuration: str, ids: list, md5_checksum: str
    ):
        self.name = name
        self.configuration = os.path.basename(configuration)
        self.ids = ids
        self.md5_checksum = md5_checksum

        # verify the checksum of the configuration file matches

        with open(configuration, "rb") as f:
            content = f.read()
            hashlib.md5().update(content)
            digest = hashlib.md5().hexdigest()
            if digest != md5_checksum:
                raise ValueError(
                    f"Subarray checksum {md5_checksum} does not match"
                    f" configuration {digest}"
                )

        # read configuration

        cols = ["X", "Y", "Z", "Diam", "Station"]

        df = ascii.read(
            os.path.expandvars(configuration),
            comment="#",
            names=cols,
        )

        df.add_column(np.arange(len(df)), name="ID", index=0)
        cols.insert(0, "ID")

        # adding in indexing as this does not work by default with astropy
        df.add_index(cols)

        # select subarray

        df_subarray = df.iloc[ids]

        # how many antennas are SKA or Meerkat?
        self.n_ska = len(fnmatch.filter(df_subarray["Station"], "SKA*"))
        self.n_meerkat = len(fnmatch.filter(df_subarray["Station"], "M*"))

    def __eq__(self, other):
        """Equality method. The configuration attribute is ignored here because
        the same file could have been loaded via a different path. The checksum
        is the real test.
        """
        return (
            self.name,
            self.ids,
            self.md5_checksum,
            self.n_meerkat,
            self.n_ska,
        ) == (
            other.name,
            other.ids,
            other.md5_checksum,
            other.n_meerkat,
            other.n_ska,
        )

    @property
    def n_MeerKAT(self):  # pylint: disable=invalid-name
        return self.n_meerkat

    @property
    def n_SKA(self):  # pylint: disable=invalid-name
        return self.n_ska


class SubarraySchema(Schema):
    """
    Schema to de/serialize the data of the Subarray class
    """

    name = fields.Str()
    configuration = fields.Str()
    ids = fields.List(fields.Int())
    md5_checksum = fields.Str()

    @post_load
    def make_subarray(self, data, **kwargs):
        return Subarray(**data)


class SubarrayStorage:
    """
    Class to handle the storage of subarrays in JSON files

    :param storage_path: path of the storage area
    :type storage_path: str
    """

    subarray_schema = SubarraySchema()

    def __init__(self, storage_path):
        """
        Initialize the storage area.
        """
        self.storage_path = Path(storage_path)

    def list(self):
        """
        List the subarray files stored
        """
        list_json_files = self.storage_path.glob("*.json")
        return sorted([name.stem for name in list_json_files])

    def load(self, name):
        """
        Load one of the subarray files stored

        :param name: name of the subarray configuration
        :type name: str
        """
        filename = self.storage_path / (name + ".json")
        if filename.is_file():
            with open(filename, "r") as f:
                subarray_data = json.load(f)
                subarray_data["configuration"] = str(
                    self.storage_path / subarray_data["configuration"]
                )
                subarray = self.subarray_schema.load(subarray_data)
                return subarray
        else:
            raise ValueError(f"Subarray {name} not found")

    # # Possible future method to store subarrays
    # def write(self, subarray, overwrite=False):
    #     filename = self.storage_path / (subarray.name + ".json")
    #     if not filename.is_file() or overwrite:
    #         json_encoded = self.subarray_schema.dumps(subarray)
    #         with open(filename, "w") as out:
    #             out.write(json_encoded)
    #
