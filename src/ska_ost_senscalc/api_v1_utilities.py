from inspect import signature
from pathlib import Path

import astropy.units as u
import yaml
from astropy.coordinates import Angle, SkyCoord

from .mid_prop import Calculator
from .mid_utilities import Beam, CalculatorMode, Limit, Weighting

STATIC_DATA_PATH = Path(__file__).resolve().parents[0] / "static"
OPEN_API_V1 = yaml.safe_load(
    open(STATIC_DATA_PATH / "openapi-mid-v1.yaml", "r")
)


def to_float(value, cast=float):
    """
    'null' values (usually for disabled inputs) are sent in the GET requests as empty strings.
    If this is the case, explicitly set the value to 'None', otherwise cast the string to a float.
    The value can be cast to a different type with the keyword argument 'cast'.

    :param value: Value
    :type value: str
    :param cast: (optional) function used to cast the value. This is float by default.
    :type cast: function
    :return: Parsed value
    :rtype: float (or type returned by 'cast')
    """
    return None if value == "" or value is None else cast(value)


def get_chunks(n_chunks, obs_freq, bandwidth):
    """Function to get the centres (and common width) of the N chunks of bandwidth

    :param n_chunks: Number of chunks
    :type n_chunks: int
    :param obs_freq: Frequency
    :type obs_freq: float
    :param bandwidth: Bandwidth
    :type bandwidth: float
    :return: A list of frequency centres for the chunks and the chunk width
    :rtype: Tuple(List, float)
    """
    left = obs_freq - (0.5 * bandwidth)
    chunk_width = bandwidth / n_chunks
    return [
        left + ((i + 0.5) * chunk_width) for i in range(n_chunks)
    ], chunk_width


def parse_parameters(request, params_definition):
    """Parse a Flask request object to get all the possible parameters
    defined on the API and validate and transform them"""
    out_params = {}
    for param in params_definition:
        name = param["name"]
        param_type = param["schema"]["type"]

        # Get value if the param is there (considering the type)
        if param_type == "array":
            array_items_type = param["schema"]["items"]["type"]
            if array_items_type == "number":
                param_value = [to_float(e) for e in request.args.getlist(name)]
            else:
                raise NotImplementedError(
                    f"The items of the array of parameter {name} are of a type"
                    f" ({param_type}) whose parsing is not implemented"
                )
        elif param_type == "number":
            param_value = to_float(request.args.get(name))
        elif param_type == "integer":
            param_value = to_float(request.args.get(name), cast=int)
        elif param_type == "string":
            param_value = request.args.get(name)
        else:
            raise NotImplementedError(
                f"The parameter {name} is of a type ({param_type}) "
                "whose parsing is not implemented"
            )

        if param.get("required") and not param_value:
            raise RuntimeError(f"The parameter {name} is required")

        if param_value or param_value == 0:  # Check if empty
            # Check limits
            if "minimum" in param["schema"]:
                minimum_value = param["schema"]["minimum"]
                if param_value < minimum_value:
                    raise ValueError(
                        f"The value of {name} ({param_value}) is below "
                        f"the minimum allowed ({minimum_value})"
                    )
            if "maximum" in param["schema"]:
                maximum_value = param["schema"]["maximum"]
                if param_value > maximum_value:
                    raise ValueError(
                        f"The value of {name} ({param_value}) is above "
                        f"the maximum allowed ({maximum_value})"
                    )
            out_params = {**out_params, name: param_value}
    return out_params


def parse_parameters_api_v1(request, endpoint="calculate"):
    """Parse a Flask request object to get all the possible parameters
    defined on the API v1 and validate and transform them
    """
    params_definition = OPEN_API_V1["paths"][f"/{endpoint}"]["get"][
        "parameters"
    ]
    return parse_parameters(request, params_definition=params_definition)


# Get the keywords of the Calculator constructor
KWARGS_CALCULATOR = [
    p.name
    for p in signature(Calculator).parameters.values()
    if p.kind == p.KEYWORD_ONLY
]


def get_calculate_response(params):
    """
    Using the parameters of the query return the appropriate values for the calculation
    """
    # Parse the target
    target = SkyCoord(
        params["ra_str"],
        params["dec_str"],
        frame="icrs",
        unit=(u.hourangle, u.deg),
    )
    params["target"] = target
    # Keep only the params in the list of constructor inputs
    constructor_params = {
        k: v for k, v in params.items() if k in KWARGS_CALCULATOR
    }
    # Main results
    result = {}

    calculator = Calculator(**constructor_params)
    line_calculator = None

    if params.get("resolution"):
        line_params = constructor_params.copy()
        line_params["bandwidth"] = params["resolution"]
        line_calculator = Calculator(**line_params)

    if params.get("integration_time") and params.get("sensitivity"):
        raise RuntimeError(
            "Only one of 'sensitivity' or 'integration_time' can be specified"
            " at once."
        )
    if params.get("integration_time"):
        sensitivity = (
            calculator.calculate_sensitivity(params["integration_time"])
            .to(u.Jy)
            .value[0]
        )
        result.update(
            {
                "result": {
                    "state": calculator.state(),
                    "sensitivity": sensitivity,
                }
            }
        )

        if line_calculator is not None:
            # Calculate line sensitivity using resolution for bandwidth
            line_sensitivity = (
                line_calculator.calculate_sensitivity(
                    params["integration_time"]
                )
                .to(u.Jy)
                .value[0]
            )
            result["result"]["line_sensitivity"] = line_sensitivity

    if params.get("sensitivity"):
        integration_time = (
            calculator.calculate_integration_time(params["sensitivity"])
            .to(u.s)
            .value[0]
        )
        result.update(
            {
                "result": {
                    "state": calculator.state(),
                    "integration_time": integration_time,
                }
            }
        )

        if line_calculator is not None:
            # Calculate line integration time using resolution for bandwidth
            line_integration_time = (
                line_calculator.calculate_integration_time(
                    params["sensitivity"]
                )
                .to(u.s)
                .value[0]
            )
            result["result"]["line_integration_time"] = line_integration_time
    # Chunks
    if params.get("n_chunks"):
        chunk_results = []
        chunk_frequencies, chunk_bandwidth = get_chunks(
            params["n_chunks"], params["frequency"], params["bandwidth"]
        )
        for chunk_frequency in chunk_frequencies:
            chunk_params = constructor_params.copy()
            chunk_params["frequency"] = chunk_frequency
            chunk_params["bandwidth"] = chunk_bandwidth
            chunk_calculator = Calculator(**chunk_params)
            if params.get("integration_time"):
                sensitivity = (
                    chunk_calculator.calculate_sensitivity(
                        params["integration_time"]
                    )
                    .to(u.Jy)
                    .value[0]
                )
                chunk_results.append(
                    {
                        "state": chunk_calculator.state(),
                        "sensitivity": sensitivity,
                    }
                )
            if params.get("sensitivity"):
                integration_time = (
                    chunk_calculator.calculate_integration_time(
                        params["sensitivity"]
                    )
                    .to(u.s)
                    .value[0]
                )
                chunk_results.append(
                    {
                        "state": chunk_calculator.state(),
                        "integration_time": integration_time,
                    }
                )
        result.update({"chunks": chunk_results})
    # Zooms
    if params.get("zoom_frequencies"):
        # Check that zoom_frequencies has the same length as zoom_resolutions
        if not params.get("zoom_resolutions"):
            raise RuntimeError("Parameter 'zoom_resolutions' must also be set")
        if len(params.get("zoom_frequencies")) != len(
            params.get("zoom_resolutions")
        ):
            raise RuntimeError(
                "Parameters 'zoom_resolutions' and 'zoom_frequencies' must"
                " have the same length"
            )
        if params.get("sensitivity") and not params.get("zoom_sensitivities"):
            # Check that zoom_sensitivities are specified if requesting a sensitivity
            raise RuntimeError("Parameter 'zoom_sensitivities' must be set")
        zoom_results = []
        if params.get("integration_time"):
            for zoom_frequency, zoom_resolution in zip(
                params["zoom_frequencies"], params["zoom_resolutions"]
            ):

                zoom_params = constructor_params.copy()
                zoom_params["frequency"] = zoom_frequency
                zoom_params["bandwidth"] = zoom_resolution
                zoom_calculator = Calculator(**zoom_params)
                sensitivity = (
                    zoom_calculator.calculate_sensitivity(
                        params["integration_time"]
                    )
                    .to(u.Jy)
                    .value[0]
                )
                zoom_results.append(
                    {
                        "state": zoom_calculator.state(),
                        "sensitivity": sensitivity,
                    }
                )

        if params.get("zoom_sensitivities"):
            if len(params.get("zoom_sensitivities")) != len(
                params.get("zoom_resolutions")
            ):
                raise RuntimeError(
                    "Parameters 'zoom_sensitivities' and 'zoom_frequencies'"
                    " must have the same length"
                )
            for zoom_frequency, zoom_resolution, zoom_sensitivity in zip(
                params["zoom_frequencies"],
                params["zoom_resolutions"],
                params["zoom_sensitivities"],
            ):
                zoom_params = constructor_params.copy()
                zoom_params["frequency"] = zoom_frequency
                zoom_params["bandwidth"] = zoom_resolution
                zoom_calculator = Calculator(**zoom_params)
                integration_time = (
                    zoom_calculator.calculate_integration_time(
                        zoom_sensitivity
                    )
                    .to(u.s)
                    .value[0]
                )
                zoom_results.append(
                    {
                        "state": zoom_calculator.state(),
                        "integration_time": integration_time,
                    }
                )
        result.update({"zooms": zoom_results})
    elif params.get("zoom_resolutions"):
        raise RuntimeError("Parameter 'zoom_frequencies' must also be set")
    return result


# Get the keywords of the weighting function
KWARGS_WEIGHTING = [
    p.name
    for p in signature(Beam).parameters.values()
    if p.kind == p.KEYWORD_ONLY
]


def get_weighting_response(params):
    """
    Using the parameters of the query return the appropriate values for weighting correction factor,
    synthesized-beam-sensitivity factor and beam shape
    """
    # Parse declination
    a = Angle(params["dec_str"] + " degrees")
    params["dec"] = a.degree

    # Parse the weighting and calculator mode
    params["weighting"] = Weighting(params["weighting"])
    params["calculator_mode"] = CalculatorMode(params["calculator_mode"])
    # Keep only the params in the list of inputs
    relevant_params = {
        k: v for k, v in params.items() if k in KWARGS_WEIGHTING
    }

    beam = Beam(**relevant_params)
    w_factor = beam.weighting_factor()
    sbsc_factor = beam.surface_brightness_conversion_factor()

    confusion_noise = beam.confusion_noise()

    # replacing all upper limits with 0
    if Limit.UPPER.value in confusion_noise[1]:
        idx = confusion_noise[1].index(Limit.UPPER.value)
        confusion_noise[0][idx] = 0
        confusion_noise[1][idx] = Limit.VALUE.value

    confusion_noise_response = {
        "value": confusion_noise[0].value,
        "limit_type": confusion_noise[1],
    }

    beam_size = beam.beam_size()
    beam_size_response = []
    for b in beam_size:
        beam_size_resp = {
            "beam_maj_scaled": b.beam_maj.value,
            "beam_min_scaled": b.beam_min.value,
            "beam_pa": b.beam_pa.value,
        }
        beam_size_response.append(beam_size_resp)

    return {
        "weighting_factor": w_factor,
        "sbs_conv_factor": sbsc_factor.value,
        "confusion_noise": confusion_noise_response,
        "beam_size": beam_size_response,
    }
