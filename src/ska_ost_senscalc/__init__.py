"""Module containing Sensitivity Calculator modules
"""
import json
from os import getenv

import connexion
from astropy.utils.misc import JsonCustomEncoder
from flask.json.provider import JSONProvider
from flask_swagger_ui import get_swaggerui_blueprint

from .api_v1 import api_v1_bp

KUBE_NAMESPACE = getenv("KUBE_NAMESPACE", "ska-ost-senscalc")
LOW_ENABLED = getenv("LOW_ENABLED", "False").lower() == "true"


class AstropyJSONProvider(JSONProvider):
    def dumps(self, obj, *args, **kwargs):
        return json.dumps(obj, cls=JsonCustomEncoder)

    def loads(self, s, **kwargs):
        return json.loads(s)


def create_app(low_enabled: bool = LOW_ENABLED):
    """
    Create the Connexion application with required config
    """
    connexion_app = connexion.App(__name__, specification_dir="static/")

    app = connexion_app.app
    app.json = AstropyJSONProvider(app)

    app.register_blueprint(api_v1_bp, url_prefix="/api/v1")

    swagger_ui_url = f"/{KUBE_NAMESPACE}/api_doc"
    mid_spec_url = f"/{KUBE_NAMESPACE}/static/openapi-mid-v1.yaml"
    swagger_urls_config = [{"url": mid_spec_url, "name": "MID API v1"}]

    if low_enabled:
        low_api_url = f"/{KUBE_NAMESPACE}/static/openapi-low-v1.yaml"
        swagger_urls_config.append({"url": low_api_url, "name": "LOW API v1"})

        connexion_app.add_api("openapi-low-v1.yaml", base_path="/api/low")

    # Use the Flask blueprint rather than letting the Connexion extra dependency handle the SwaggerUI.
    # This is because our k8s ingress rules require the SwaggerUI to make requests to a different base path
    # than the spec is registered with above.
    swaggerui_blueprint = get_swaggerui_blueprint(
        swagger_ui_url,
        mid_spec_url,
        config={
            "app_name": "Sensitivity Calculator API",
            "urls": swagger_urls_config,
            "showCommonExtensions": True,
        },
    )
    app.register_blueprint(swaggerui_blueprint, url_prefix=swagger_ui_url)

    return connexion_app
