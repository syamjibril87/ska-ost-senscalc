"""
Module holding functions useful to the MidCalculator
"""
import logging
from enum import Enum

logger = logging.getLogger("senscalc")

from math import sqrt

import astropy.units as u
import numpy as np
from astropy.constants import c, k_B
from astropy.table import Table
from scipy.interpolate import interp1d

from ska_ost_senscalc.utilities import (
    MEERKAT_DISH_POINTING_ERROR,
    SKA_DISH_POINTING_ERROR,
    STATIC_DATA_PATH,
    Atmosphere,
    Celestial,
    DishType,
    TelParams,
    Utilities,
)

# pylint: disable=invalid-name

celestial = Celestial()


def read_table(file_name, columns=None):
    """
    function to read in the lookup tables
    """
    path_to_table = STATIC_DATA_PATH / "lookups" / file_name
    return Table.read(path_to_table, include_names=columns)


BEAM_WEIGHTING_COLUMNS = [
    "subarray",
    "weighting",
    "robustness",
    "taper_arcsec",
    "declination",
    "spec_mode",
    "pss",
    "cleanbeam_bmaj",
    "cleanbeam_bmin",
    "cleanbeam_bpa",
    "reltonat_casa",
]

weighting_table = read_table(
    "beam_weighting_sensitivity.csv", columns=BEAM_WEIGHTING_COLUMNS
)

confusion_noise_table = read_table("confusion_noise.ecsv")

# original look up table in linear space, converting the table to log space for
# the calculation
for k in confusion_noise_table.keys():
    confusion_noise_table[k] = np.log10(confusion_noise_table[k])


class Weighting(Enum):
    """
    Enumeration for different weighting
    """

    NATURAL = "natural"
    ROBUST = "robust"
    UNIFORM = "uniform"


class CalculatorMode(Enum):
    """
    Enumeration for calculator modes
    """

    LINE = "line"
    CONTINUUM = "continuum"
    PULSAR = "pulsar"


class Limit(Enum):
    """
    Enumeration for different types of limit
    """

    UPPER = "upper limit"
    LOWER = "lower limit"
    VALUE = "value"


class BeamSize(object):
    """Class for returning a BeamSize object.

    :param beam_maj: the beam major axis in degrees
    :type beam_maj: astropy.units.Quantity or number
    :param beam_min: the beam minor axis in degrees
    :type beam_min: astropy.units.Quantity or number
    :param beam_pa: the beam position angle in degrees
    :type beam_pa: astropy.units.Quantity or number
    :return: BeamSize object
    :rtype: object
    """

    def __init__(self, beam_maj, beam_min, beam_pa):
        """
        BeamSize class constructor
        """

        self.beam_maj = Utilities.to_astropy(beam_maj, u.deg)
        self.beam_min = Utilities.to_astropy(beam_min, u.deg)
        self.beam_pa = Utilities.to_astropy(beam_pa, u.deg)

    def __repr__(self):
        return f"<BeamSize({self.beam_maj}, {self.beam_min}, {self.beam_pa})>"


def eta_bandpass():
    """Efficiency factor for due to the departure of the bandpass from an ideal, rectangular
    shape. For now this is a placeholder.

    :return: the efficiency, eta
    :rtype: float
    """
    eta = 1.0
    logger.debug(f"eta_bandpass() -> {eta}")
    return eta


def eta_coherence(obs_freq):
    """Efficiency factor for the sensitivity degradation due to the
    incoherence on a baseline.

    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :return: the efficiency, eta
    :rtype: float
    """
    eta = np.exp(np.log(0.98) * obs_freq.to_value(u.GHz) ** 2 / 15.4**2)
    logger.debug(f"eta_coherence({obs_freq}) -> {eta}")
    return eta


def eta_correlation():
    """Efficiency factor due to imperfection in the correlation algorithm, e.g. truncation error.

    :return: the efficiency, eta
    :rtype: float
    """
    eta = 0.98
    logger.debug(f"eta_correlation() -> {eta}")
    return eta


def eta_digitisation(obs_band):
    """Efficiency factor due to losses from quantisation during signal digitisation.
    This process is independent of the telescope and environment, but only depends on
    the 'effective number of bits' (ENOB) of the system, which depends in turn on
    digitiser quality and clock jitter, and on band flatness.

    :param obs_band: the observing band
    :type obs_band: str
    :return: the efficiency, eta
    :rtype: float
    """

    if obs_band == "Band 1":
        eta = 0.999
    elif obs_band == "Band 2":
        eta = 0.999
    elif obs_band == "Band 3":
        eta = 0.998
    elif obs_band == "Band 4":
        eta = 0.98
    elif obs_band == "Band 5a":
        eta = 0.955
    elif obs_band == "Band 5b":
        eta = 0.955
    else:
        raise RuntimeError("bad obs_band: %s" % obs_band)

    logger.debug(f"eta_digitisation() -> {eta}")
    return eta


def eta_dish(obs_freq, dish_type):
    """Efficiency factor due to losses for specified dish type.

    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :param dish_type: the type of dish
    :type dish_type: DishType
    :return: the efficiency, eta
    :rtype: float
    """

    eta = TelParams.calculate_dish_efficiency(obs_freq, dish_type)
    logger.debug(f"eta_dish({obs_freq}, {dish_type}) -> {eta}")
    return eta


def eta_point(obs_freq, dish_type):
    """Efficiency factor at the observing frequency due to the dish pointing error.

    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :param dish_type: the type of dish
    :type dish_type: DishType
    :return: the efficiency, eta
    :rtype: float
    """
    if dish_type is DishType.SKA1:
        pointing_error = SKA_DISH_POINTING_ERROR
    elif dish_type is DishType.MeerKAT:
        pointing_error = MEERKAT_DISH_POINTING_ERROR
    else:
        raise RuntimeError("bad dish_type: %s" % dish_type)

    eta = 1.0 / (
        1.0
        + (
            8
            * np.log(2)
            * (pointing_error / TelParams.dish_fwhm(obs_freq, dish_type)) ** 2
        )
    )
    eta = eta.to_value(u.dimensionless_unscaled)
    logger.debug(f"eta_point({obs_freq}, {dish_type}) -> {eta}")
    return eta


# Not currently used.
def eta_rfi():
    """Efficiency factor due to Radio Frequency Interference (RFI)

    :return: the efficiency, eta
    :rtype: float
    """
    eta = 1.00
    logger.debug(f"eta_rfi() -> {eta}")
    return eta


def eta_system(
    eta_point, eta_coherence, eta_digitisation, eta_correlation, eta_bandpass
):
    """System efficiency for SKA interferometer

    :param eta_point: efficiency loss due to pointing errors
    :type eta_point: float
    :param eta_coherence: efficiency due to loss of coherence
    :type eta_coherence: float
    :param eta_digitisation: efficiency loss due to errors in the digitisation process
    :type eta_digitisation: float
    :param eta_correlation: efficiency loss due to errors in the correlation process
    :type eta_correlation: float
    :param eta_bandpass: efficiency loss due to the bandpass not being rectangular
    :type eta_bandpass: float

    :return: the system efficiency
    :rtype: float
    """

    eta_system = (
        eta_point
        * eta_coherence
        * eta_digitisation
        * eta_correlation
        * eta_bandpass
    )
    logger.debug(
        f"eta_system({eta_point}, {eta_coherence}, {eta_digitisation},"
        f" {eta_correlation}, {eta_bandpass}) -> {eta_system}"
    )
    return eta_system


def Tgal(target, obs_freq, dish_type, alpha):
    """Brightness temperature of Galactic background in target direction at observing frequency.

    :param target: target direction
    :type target: astropy.SkyCoord
    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :param dish_type: the type of dish
    :type dish_type: DishType
    :param alpha: spectral index of emission
    :type alpha: float
    :return: the brightness temperature of the Galactic background
    :rtype: astropy.units.Quantity
    """

    result = celestial.calculate_Tgal(target, obs_freq, dish_type, alpha)
    logger.debug(
        f"Tgal({target}, {obs_freq}, {dish_type}, {alpha}) -> {result}"
    )
    return result


def Trcv(obs_freq, obs_band, dish_type):
    """Receiver temperature for specified freq, band and dish.

    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :param obs_band: the observing band
    :type obs_band: str
    :param dish_type: the type of dish
    :type dish_type: DishType
    :return: the receiver temperature
    :rtype: astropy.units.Quantity
    """

    result = TelParams.calculate_Trcv(obs_freq, obs_band, dish_type)
    logger.debug(f"Trcv({obs_freq}, {obs_band}, {dish_type}) -> {result}")
    return result


def Tsky(Tgal, obs_freq, elevation, weather):
    """Brightness temperature of sky in target direction.

    :param Tgal: brightness temperature of Galactic background
    :type Tgal: astropy.units.Quantity
    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :param elevation: the observing elevation
    :type elevation: astropy.units.Quantity
    :param weather: the atmosphere PWV
    :type weather: float
    :return: the brightness temperature of the sky
    :rtype: astropy.units.Quantity
    """

    # Tatm, Tcmb cannot be overriden by the user
    # Tatm at zenith
    Tatm_zen = Atmosphere.calculate_Tatm(weather, obs_freq)

    # Tcmb and Tgal are attenuated by the atmosphere,
    # Tatm also varies with zenith angle
    tau_zen = Atmosphere.get_tauz_atm(weather, obs_freq)

    zenith = 90.0 * u.deg - elevation
    tau = tau_zen / np.cos(zenith)

    # approximating Tatm ~ Tphys * (1 - exp(-tau))
    Tphys = Tatm_zen / (1 - np.exp(-tau_zen))

    result = Tphys * (1 - np.exp(-tau)) + (Tgal + celestial.Tcmb) * np.exp(
        -tau
    )
    logger.debug(
        f"Tsky({Tgal}, {obs_freq}, {elevation}, {weather}) -> {result}"
    )
    return result


def Tspl(dish_type):
    """Spillover temperature for specified dish type.

    :param dish_type: the type of dish
    :type dish_type: DishType
    :return: the spillover temperature
    :rtype: astropy.units.Quantity
    """

    result = TelParams.calculate_Tspl(dish_type)
    logger.debug(f"Tspl() -> {result}")
    return result


def Tsys_dish(Trcv, Tspl, Tsky, obs_freq):
    """System temperature.

    :param Trcv: the receiver temperature
    :type Trcv: astropy.units.Quantity
    :param Tspl: the spillover temperature
    :type Tspl: astropy.units.Quantity
    :param Tsky: the sky temperature
    :type Tsky: astropy.units.Quantity
    :param obs_freq: the observing frequency
    :type obs_freq: astropy.units.Quantity
    :return: the dish system temperature
    :rtype: astropy.units.Quantity
    """

    # Add the temperatures to get the system temperature on the ground
    Tsys_ground = Trcv + Tspl + Tsky

    # Apply high-frequency correction to Rayleigh-Jeans temperature
    result = Utilities.Tx(obs_freq, Tsys_ground)
    result = result.to(u.K)
    logger.debug(f"Tsys_dish({Trcv}, {Tspl}, {Tsky}, {obs_freq}) -> {result}")

    return result


class Beam:
    """Beam class

    :param frequency: the observing frequency (Hz)
    :type frequency: astropy.units.Quantity or number
    :param zoom_frequencies: the frequencies of the line zooms (Hz)
    :type zoom_frequencies: iterable astropy.units.Quantity or list
    :param array_configuration: the subarray configuration
    :type array_configuration: str, "full" "SKA1 (133 x 15m)" or "meerKAT"
    :param weighting: the type of uv-weighting used (robust or uniform)
    :type weighting: Weighting
    :param robustness: the Briggs robustness parameter
    :type robustness: float
    :param dec: the target declination (deg)
    :type dec: astropy.units.Quantity or number
    :param taper: the Gaussian taper to apply (arcsec)
    :type taper: astropy.units.Quantity or number
    :param calculator_mode: the calculator mode being used
    :type calculator_mode: CalculatorMode
    :return: Beam object
    :rtype: object
    """

    def __init__(
        self,
        *,
        frequency=None,
        zoom_frequencies=None,
        array_configuration=None,
        weighting=None,
        robustness=None,
        dec=None,
        taper=0.0,
        calculator_mode=None,
    ):

        if frequency is None and zoom_frequencies is None:
            raise RuntimeError(
                f"Parameter 'frequency' or 'zoom_frequencies' is required"
            )
        elif frequency is None and calculator_mode is CalculatorMode.CONTINUUM:
            raise RuntimeError(
                "Parameter 'frequency' must be set for calculator mode:"
                f" {calculator_mode}"
            )
        else:
            self.frequency = Utilities.to_astropy(frequency, u.Hz)

        if zoom_frequencies is None and calculator_mode is CalculatorMode.LINE:
            raise RuntimeError(
                "Parameter 'zoom_frequencies' must be set for calculator"
                f" mode: {calculator_mode}"
            )
        else:
            self.zoom_frequencies = Utilities.to_astropy(
                zoom_frequencies, u.Hz
            )

        if dec is None:
            raise RuntimeError("Parameter 'dec' is required")
        else:
            self.dec = Utilities.to_astropy(dec, u.deg)

        if weighting is None:
            raise RuntimeError("Parameter 'weighting' is required")
        elif weighting not in [
            Weighting.UNIFORM,
            Weighting.ROBUST,
            Weighting.NATURAL,
        ]:
            raise RuntimeError(f"Invalid weighting option: {weighting}")
        elif (weighting == Weighting.ROBUST) and robustness is None:
            raise RuntimeError(
                f"Parameter 'robustness' should be set for 'robust' weighting"
            )
        elif (weighting == Weighting.ROBUST) and robustness not in np.arange(
            -2, 3
        ):
            raise RuntimeError(
                f"Invalid value for parameter 'robustness': {robustness}"
            )
        else:
            self.weighting = weighting
            if weighting != Weighting.ROBUST and robustness:
                logger.warning(
                    f"Robust parameter defined ({robustness}) but will be"
                    f" ignored for given weighting ({weighting.name})"
                )
            self.robustness = robustness

        if array_configuration is None:
            raise RuntimeError("Parameter 'array_configuration' is required")
        elif array_configuration not in [
            "full",
            "SKA1 (133 x 15m)",
            "meerKAT",
        ]:
            raise RuntimeError(
                f"Invalid array configuration: {array_configuration}"
            )
        else:
            self.array_configuration = array_configuration

        if isinstance(taper, u.quantity.Quantity):
            taper_arcsec = taper.to(u.arcsec)
        else:
            taper_arcsec = taper * u.arcsec
        if taper_arcsec.value not in [
            0,
            0.25,
            0.5,
            1.0,
            2.0,
            4.0,
            8.0,
            16.0,
            32.0,
            64.0,
            128.0,
            256.0,
            512.0,
            1024.0,
            1280.0,
        ]:
            raise RuntimeError(f"Invalid value of tapering:  {taper_arcsec}")
        else:
            self.taper = taper_arcsec

        if calculator_mode is None:
            raise RuntimeError("Parameter 'calculator_mode' is required")
        elif calculator_mode not in [
            CalculatorMode.LINE,
            CalculatorMode.CONTINUUM,
            # CalculatorMode.PULSAR, commented out for now as currently not supported
        ]:
            raise RuntimeError(f"Invalid calculator mode: {calculator_mode}")
        else:
            self.calculator_mode = calculator_mode

    def beam_size(self):
        """
        method to return list of beam sizes.
        """

        weighted = self.__filter_table()

        interp_bmaj = interp1d(
            weighted["declination"], weighted["cleanbeam_bmaj"]
        )
        interp_bmin = interp1d(
            weighted["declination"], weighted["cleanbeam_bmin"]
        )
        interp_bpa = interp1d(
            weighted["declination"], weighted["cleanbeam_bpa"]
        )

        beam_maj = interp_bmaj(self.dec) * u.deg
        beam_min = interp_bmin(self.dec) * u.deg
        beam_pa = interp_bpa(self.dec)

        obj = []
        if self.calculator_mode is CalculatorMode.LINE:
            frequencies = self.zoom_frequencies
        else:
            frequencies = [self.frequency]

        for freq in frequencies:
            obj.append(
                BeamSize(
                    beam_maj=beam_maj * 1.4 * u.GHz / freq.to(u.GHz),
                    beam_min=beam_min * 1.4 * u.GHz / freq.to(u.GHz),
                    beam_pa=beam_pa,
                )
            )

        return obj

    def weighting_factor(self):
        """
        method to calculate the beam weighting factor of the observation
        """
        # As of PI#15 we are no longer interpolating over frequency, elevation or integration time;
        # only declination. This has considerably simplified this method. If interpolation over more
        # than one parameter is required see commit 3640f432

        if (
            self.weighting is Weighting.NATURAL
            and self.taper == 0.0 * u.arcsec
        ):
            return 1.0

        else:
            weighted = self.__filter_table()

            limits = (
                weighted["declination"].min(),
                weighted["declination"].max(),
            )

            # Raise RuntimeError if declination value is outside the lookup values (extrapolating)
            if limits[1] <= self.dec.value <= limits[0]:
                raise RuntimeError(
                    "Cannot extrapolate, given value of declination is"
                    f"beyond min {limits[0]} and max {limits[1]} values"
                )

            natural = self.__filter_table(
                use_weighting=Weighting.NATURAL, use_taper=0.0 * u.arcsec
            )

            interp_weighted = interp1d(
                weighted["declination"], weighted["pss"]
            )
            interp_natural = interp1d(natural["declination"], natural["pss"])

            return interp_weighted(self.dec) / interp_natural(self.dec)

    def surface_brightness_conversion_factor(self):
        """
        method to calculate the factor the sensitivity returned by calculator must be multiplied by
        to obtain the surface brightness sensitivity in units of K/Jy.
        """
        if self.calculator_mode is CalculatorMode.LINE:
            frequency = self.zoom_frequencies
        else:
            frequency = self.frequency

        wavelength = c / frequency.to(u.s**-1)
        factor = (
            wavelength**2 / (2.0 * k_B * self.__area()) * (1 * u.sr)
        )  # fudge factor to remove the /sr

        return factor.to(u.K / u.Jy)

    def confusion_noise(self):
        """
        method to calculate the confusion noise of beam
        """
        confusion_noise = []
        lim = []

        if self.calculator_mode == CalculatorMode.LINE:
            frequency = self.zoom_frequencies.value
        else:
            frequency = [self.frequency.value]

        log_frequency = np.log10(frequency)

        beam_geomean = []
        for beam in self.beam_size():
            beam_geomean.append(
                sqrt(beam.beam_min.value * beam.beam_maj.value)
            )

        log_beam_geomean = np.log10(beam_geomean)

        sigma_min = confusion_noise_table["sigma"].min(axis=0)
        sigma_max = confusion_noise_table["sigma"].max(axis=0)

        for n, log_f in enumerate(log_frequency):

            sigma_m = interp1d(
                confusion_noise_table["beam"],
                confusion_noise_table["sigma"],
                bounds_error=False,
                axis=0,
                fill_value=(sigma_min, sigma_max),
            )(log_beam_geomean[n])

            sigma = interp1d(
                confusion_noise_table["frequency"][0],
                sigma_m,
                fill_value="extrapolate",
            )(log_f)
            confusion_noise.append(10**sigma)

            if log_beam_geomean[n] < confusion_noise_table["beam"].min():
                lim.append(Limit.UPPER.value)
            elif log_beam_geomean[n] > confusion_noise_table["beam"].max():
                lim.append(Limit.LOWER.value)
            else:
                lim.append(Limit.VALUE.value)

        return confusion_noise * u.Jy, lim

    def __filter_table(self, use_weighting=None, use_taper=None):
        """
        private method to filter the lookup table
        """
        subarray = self.array_configuration + ".json"

        if use_weighting is None:
            use_weighting = self.weighting

        if use_taper is None:
            use_taper = self.taper
        elif isinstance(use_taper, u.quantity.Quantity):
            use_taper = use_taper.to(u.arcsec)
        else:
            use_taper = use_taper * u.arcsec

        weighted = weighting_table[
            (
                (weighting_table["subarray"] == subarray)
                & (weighting_table["weighting"] == use_weighting.value)
                & (weighting_table["spec_mode"] == self.calculator_mode.value)
                & (weighting_table["taper_arcsec"] == use_taper.value)
            )
        ]

        # Only take robustness into account if weighting is `robust`
        if use_weighting == Weighting.ROBUST:
            weighted = weighted[(weighted["robustness"] == self.robustness)]

        return weighted

    def __area(self):
        """
        private method to calculate the beam area
        """
        beam = self.beam_size()
        area = []
        for b in beam:
            theta = np.sqrt(b.beam_maj * b.beam_min).to(u.rad)
            area_sr = (np.pi * theta**2 / (4 * np.log(2))).to(u.sr).value
            area.append(area_sr)

        return area * u.sr
