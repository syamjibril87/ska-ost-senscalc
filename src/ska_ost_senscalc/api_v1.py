from flask import Blueprint, jsonify, make_response, request
from flask.json import dumps

from ska_ost_senscalc.subarray import STORAGE_PATH, SubarrayStorage

from .api_v1_utilities import (
    get_calculate_response,
    get_weighting_response,
    parse_parameters_api_v1,
)

subarray_storage = SubarrayStorage(STORAGE_PATH)

api_v1_bp = Blueprint("api_v1", __name__, template_folder="templates")


# AT2-1002: This error handler cannot be tested in debug mode. The informative Werkzeug error
#  page is displayed instead.
@api_v1_bp.errorhandler(500)
def internal_server_error(e):
    result_output = {
        "status": "error",
        "message": "Internal server error - " + repr(e),
    }
    return make_response(jsonify(result_output), 500)


@api_v1_bp.route("/calculate")
def calculate():
    try:
        parameters = parse_parameters_api_v1(request, "calculate")
        result = get_calculate_response(parameters)
    except (NotImplementedError, RuntimeError, ValueError) as e:
        result_output = {
            "status": "fail",
            "message": "Error parsing the parameters - " + repr(e),
        }
        return make_response(jsonify(result_output), 400)
    # Custom JSON encoders takes care of arrays
    result_output = {"status": "success", "data": result}
    response = make_response(dumps(result_output), 200)
    response.headers["Content-Type"] = "application/json"
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


@api_v1_bp.route("/weighting")
def weighting():
    # AT2-995: Note that the following code is similar to that in the calculate function.
    #  This could be refactored in the future in the schema of the code is repeated.
    try:
        parameters = parse_parameters_api_v1(request, "weighting")
        result = get_weighting_response(parameters)
    except (NotImplementedError, RuntimeError, ValueError) as e:
        result_output = {
            "status": "fail",
            "message": "Error parsing the parameters - " + repr(e),
        }
        return make_response(jsonify(result_output), 400)
    # Custom JSON encoders takes care of arrays
    result_output = {"status": "success", "data": result}
    response = make_response(dumps(result_output), 200)
    response.headers["Content-Type"] = "application/json"
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


@api_v1_bp.route("/subarrays")
def get_list_subarrays():
    """Entry point for the Subarrays REST API:
    Lists the available subarrays.
    """
    list_subarrays = subarray_storage.list()
    response = make_response(jsonify(list_subarrays), 200)
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response
