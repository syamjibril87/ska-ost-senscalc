import dataclasses


@dataclasses.dataclass
class CalculatorInput:
    _: dataclasses.KW_ONLY
    freq_centre: float
    bandwidth: float
    num_stations: int
    pointing_centre: str
    duration: float


@dataclasses.dataclass
class CalculatorResult:
    sensitivity: float
    units: str
