"""
These functions map to the API paths, with the returned value being the API response

Connexion maps the function name to the operationId in the OpenAPI document path
"""
import logging
from http import HTTPStatus

from ska_ost_senscalc.low.service import convert_input_and_calculate
from ska_ost_senscalc.low.validation import validate_and_set_defaults

LOGGER = logging.getLogger("senscalc")


def calculate_sensitivity(**kwargs):
    """
    Function which HTTP GET requests to /api/low/calculate are routed to.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body and status, which Connexion will wrap into a Response
    """
    LOGGER.debug(
        "Request received for LOW sensitivity for input parameters %s", kwargs
    )
    try:
        validated_params = validate_and_set_defaults(kwargs)
        return convert_input_and_calculate(validated_params), HTTPStatus.OK
    except ValueError as err:
        return {"detail": err.args}, HTTPStatus.BAD_REQUEST
    except Exception:
        LOGGER.exception("Exception occurred with LOW calculate_sensitivity")
        return {
            "detail": "Internal Server Error"
        }, HTTPStatus.INTERNAL_SERVER_ERROR
