"""
The service layer is responsible for turning validated inputs into the relevant calculation inputs,
calling any calculation functions and collating the results.
"""

from ska_ost_senscalc.low.bright_source_lookup import BrightSourceCatalog
from ska_ost_senscalc.low.calculator import calculate_sensitivity
from ska_ost_senscalc.low.model import CalculatorInput

FLUX_DENSITY_THRESHOLD_JY = 10.0


def convert_input_and_calculate(user_input: dict) -> dict:
    """
    :param user_input: A kwarg dict of the HTTP parameters sent by the user
    :return: a dict containing the calculated sensitivity and its units
    """
    calculator_input = CalculatorInput(
        freq_centre=user_input["freq_centre"],
        bandwidth=user_input["bandwidth"],
        num_stations=user_input["num_stations"],
        pointing_centre=user_input["pointing_centre"],
        duration=user_input["duration"],
    )
    result = calculate_sensitivity(calculator_input)
    response = {"sensitivity": result.sensitivity, "units": result.units}
    mwa_cat = BrightSourceCatalog(threshold_jy=FLUX_DENSITY_THRESHOLD_JY)
    if mwa_cat.check_for_bright_sources(
        calculator_input.pointing_centre, calculator_input.freq_centre
    ):
        response["warn_msg"] = (
            "The specified pointing contains at least one source brighter "
            + f"than {FLUX_DENSITY_THRESHOLD_JY} Jy. Your observation may be "
            + "dynamic range limited."
        )
    return response
