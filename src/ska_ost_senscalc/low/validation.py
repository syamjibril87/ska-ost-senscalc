"""
This module provides semantic validation for inputs to the Sensitivity Calculator,
including checking for required values, setting default values, and domain related checks.

Syntactic validation and basic validation, for example of min/max values of numbers, is done
by Connexion and the OpenAPI spec.
"""
import astropy.units as u
from astropy.coordinates import SkyCoord

DEFAULT_PARAMS = {
    "num_stations": 512,
    "freq_centre": 200,
    "bandwidth": 300,
    "pointing_centre": "10:00:00 -30:00:00",
    "duration": 1,
}


def validate_and_set_defaults(user_input: dict) -> dict:
    """
    :param user_input: the parameters from the HTTP request
    :return: A new copy of the input dict, with defaults set for missing values
    :raises: ValueError if the input data is not valid
    """

    # Merge the default params and the user input into a new dict. The union operator for a dict will
    # take the rightmost value, ie if the user_input contains a key then it will not be overwritten by the defaults
    user_input = DEFAULT_PARAMS | user_input

    err_msgs = []
    min_freq = user_input["freq_centre"] - user_input["bandwidth"] / 2
    max_freq = user_input["freq_centre"] + user_input["bandwidth"] / 2
    if min_freq < 50 or max_freq > 350:
        err_msgs.append(
            "Spectral window defined by central frequency and bandwidth does"
            " not lie within the 50 - 350 MHz range."
        )

    try:
        SkyCoord(user_input["pointing_centre"], unit=(u.hourangle, u.deg))
    except ValueError:
        err_msgs.append(
            "Specified pointing centre is invalid, expected format HH:MM:SS"
            " DD:MM:SS."
        )

    if err_msgs:
        raise ValueError(*err_msgs)

    return user_input
